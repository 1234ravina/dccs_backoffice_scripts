import unittest
import time
import os
from selenium.common.exceptions import WebDriverException
from Tests.Utility import Utility
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class Tests_InstallDevice(unittest.TestCase, Utility):

    ##############################
    # Before and after tests
    ###############

    @classmethod
    def setUpClass(inst): inst.setUpActions(inst)

    @classmethod
    def tearDownClass(inst): inst.tearDownActions(inst)

    ##############################
    # Test cases
    ###############

    #####
    # Navigate to the Install Portal page
    def test_6_01_loginInstallPortal(self):
        try:
            self.driver.find_element_by_link_text("Admin").click()
            self.driver.find_element_by_link_text("Install Device").click()

            self.assertEqual(self.driver.current_url, self.backUrl + "install-portal/install")

            ### If you need to go over on the install portal URL in another tab
            ### but the code to switch between tabs isn't implemented, so be warned
            # =======================================================================
            # self.driver.execute_script("window.open('"+self.urlInstall+"')")
            # Wait(self.driver, 10).until(EC.number_of_windows_to_be(2))
            # self.driver.switch_to_window(self.driver.window_handles[1])
            # self.driver.find_element_by_id('username').send_keys(self.installCred)
            # self.driver.find_element_by_id('password').send_keys(self.installCred)
            # self.driver.find_element_by_id('login-button').click()
            # self.assertTrue(self.wait_for_url(self.urlInstall+"home"),"The user couldn't sign in")
            # =======================================================================
        except WebDriverException:
            self.fail()

    #####
    # Verify the behavior when searching invalid devices
    def test_6_02_searchWrongDevice(self):
        try:
            searchInput = self.driver.find_element_by_id('data-field')
            nextButton = self.driver.find_element_by_id('next2-button')

            # Verifying that the input is in an invalid state when invalid format is entered
            searchInput.send_keys(self.deactivatedDeviceID[:-3] + Keys.ENTER)
            self.assertTrue("ng-invalid" in searchInput.get_attribute('class'))
            self.assertFalse(nextButton.is_enabled())

            # adding some number to generate an invalid ID but well formated
            searchInput.send_keys("111")
            nextButton.click()
            self.assertTrue(self.driver.find_element_by_xpath(
                '//*[contains(.,"This Air ID is either invalid, does not exist or does not belong to this account")]').is_displayed())

            # Verify that the input doesn't receive more that the maximum character limit
            searchInput.send_keys(self.deactivatedDeviceID[-3:] + "123")
            self.assertEqual(len(searchInput.get_attribute('value')), 10)
            self.assertTrue(self.driver.find_element_by_id('next2-button').is_enabled())
        except WebDriverException:
            self.fail()

    #####
    # Searching a valid device
    def test_6_03_searchActiveDevice(self):
        try:
            # Clearing the input and searching for the given device and looking for the disclaimer
            self.driver.find_element_by_id('data-field').clear()
            self.driver.find_element_by_id('data-field').send_keys(self.deactivatedDeviceID)
            self.driver.find_element_by_id('next2-button').click()
            self.assertTrue(self.driver.find_element_by_xpath("//*[contains(.,'purchased and used')]").is_displayed())
            self.assertTrue(self.driver.find_element_by_id('next-button').is_displayed())
        except WebDriverException:
            self.fail()

    def test_6_04_backConfirmedDevice(self):
        try:
            # Making sure going Back emptied the input
            self.driver.find_element_by_id('cancel-installation-button').click()
            self.assertFalse(self.driver.find_element_by_id('cancel-installation-button').is_enabled())
            self.assertEqual(self.driver.find_element_by_id('data-field').get_attribute('value'), "")
        except WebDriverException:
            self.fail()

    def test_6_05_selectDevice(self):
        try:
            # Confirming a valid device
            self.driver.find_element_by_id('data-field').send_keys(self.deactivatedDeviceID)
            self.driver.find_element_by_id('next2-button').click()
            self.driver.find_element_by_id('next-button').click()
            Wait(self.driver, 90).until_not(EC.visibility_of_element_located((By.CLASS_NAME, 'spinner')))
            self.assertTrue(self.driver.find_element_by_xpath(
                "//span[contains(.,'AirId: " + self.deactivatedDeviceID + "')]").is_displayed())
        except WebDriverException:
            self.fail()

    #####
    # Verify that all options have customization
    def test_6_06_customizeDevice(self):
        try:
            self.driver.find_element_by_id('configuration').click()
            customizations = ['Lock', 'Unlock', 'Start', 'Trunk', 'Panic', 'Aux1', 'Aux2']
            for i in customizations:
                self.assertTrue(self.driver.find_element_by_xpath(
                    "//tr//img[contains(@src,'" + i + "')]/ancestor::td/following-sibling::td[2]//div[contains(@class,'ui-dropdown')]").is_enabled())
        except WebDriverException:
            self.fail()

    def test_6_071_statusButton(self):
        try:
            self.driver.find_element_by_id("test-status").click()
            Wait(self.driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//td[contains(.,'Remote Starter')]")))
        except:
            self.fail("The Status was not updated on clicking the button")

    def test_6_07_testAllButton(self):
        try:
            self.driver.find_element_by_id("test-all").click()
            self.assertTrue(self.is_element_present(By.ID,"test-cancel"))
        except WebDriverException:
            self.fail()

    '''def test_6_08_cancelTests(self):
        try:
            self.driver.find_element_by_id("test-cancel").click()
            self.assertTrue((self.is_element_present(By.XPATH,"//span[contains(.,'Cancelling Tests')]")))
        except WebDriverException:
            self.fail()'''

    def test_6_09_checkLockTestStatus(self):
        # This test is coded for an active device and thus will pass if all tests say "successful"
        try :
            Wait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//span[contains(.,'Lock successful')]")))
        except:
            self.fail("The Lock test failed")

    def test_6_10_checkUnlockTestStatus(self):
        # This test is coded for an active device and thus will pass if all tests say "successful"
        try:
            Wait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, "//span[contains(.,'Unlock successful')]")))
        except:
            self.fail("The Unlock test failed")

    def test_6_11_ignitionStartStatus(self):
        # This test is coded for an active device and thus will pass if all tests say "successful"
        try:
            Wait(self.driver, 20).until(
                EC.presence_of_element_located((By.XPATH, "//span[contains(.,'Ignition start successful')]")))
        except:
            self.fail("The Ignition start test failed")

    def test_6_12_ignitionOffStatus(self):
        # This test is coded for an active device and thus will pass if all tests say "successful"
        try:
            Wait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//span[contains(.,'Ignition off successful')]")))
        except:
            self.fail("The Ignition Off test failed")

    def test_6_13_trunkOpenStatus(self):
        # This test is coded for an active device and thus will pass if all tests say "successful"
        try:
            Wait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//span[contains(.,'Trunk open successful')]")))
        except:
            self.fail("The Trunk open test failed")

    def test_6_14_panicActivationStatus(self):
        # This test is coded for an active device and thus will pass if all tests say "successful"
        try:
            Wait(self.driver, 10).until(
                EC.presence_of_element_located((By.XPATH, "//span[contains(.,'Panic activation successful')]")))
        except:
            self.fail("The Panic activation test failed")

    def test_6_15_aux1Status(self):
        # This test is coded for an active device and thus will pass if all tests say "successful"
        try:
            Wait(self.driver, 30).until(
                EC.presence_of_element_located((By.XPATH, "//span[contains(.,'Auxiliary1 activation successful')]")))
        except:
            self.fail("The Aux1 test failed")

    def test_6_16_aux2Status(self):
        # This test is coded for an active device and thus will pass if all tests say "successful"
        try:
            Wait(self.driver, 30).until(
                EC.presence_of_element_located((By.XPATH, "//span[contains(.,'Auxiliary2 activation successful')]")))
        except:
            self.fail("The Aux2 test failed")

    def test_6_17_wrongCustomerInformation(self):
        try:
            Wait(self.driver, 30).until(
                EC.presence_of_element_located((By.ID, "next2-button")))
            self.driver.find_element_by_id("next2-button").click()
            self.driver.find_element_by_xpath("//app-input-field[@id='alertEmail']//input").send_keys("s5895")
            self.driver.find_element_by_xpath("//app-input-field[@id='alertPhone']//input").send_keys("555")
            self.assertTrue(self.driver.find_element_by_id("next-button").is_enabled())
            self.driver.find_element_by_id("next-button").click()
            self.assertTrue(self.is_element_present(By.XPATH, "//span[contains(.,'Error')]"), "The Error message for WRONGLY entered email and phone is shown")
        except WebDriverException:
            self.fail()

    def test_6_18_customerInformation(self):
        try:
            Wait(self.driver, 30).until(
                EC.presence_of_element_located((By.ID, "customer-finish-button"))) #remove this line if the previous test passes
            self.driver.find_element_by_id("back-button").click() #remove this line if the previous test passes
            self.driver.find_element_by_xpath("//app-input-field[@id='alertEmail']//input").clear()
            self.driver.find_element_by_xpath("//app-input-field[@id='alertEmail']//input").send_keys("dccs.automation@gmail.com")
            self.driver.find_element_by_xpath("//app-input-field[@id='alertPhone']//input").clear()
            self.driver.find_element_by_xpath("//app-input-field[@id='alertPhone']//input").send_keys("5149335656")
            self.driver.find_element_by_id("additional-info-button").click()
            self.assertTrue(self.driver.find_element_by_xpath("//label[contains(.,'Language')]"))
            self.assertTrue(self.driver.find_element_by_xpath("//label[contains(.,'Partner Branding')]"))
            self.assertTrue(self.driver.find_element_by_xpath("//label[contains(.,'Partner Branding')]"))
            self.assertTrue(self.driver.find_element_by_xpath("//label[contains(.,'VIN')]"))
        except WebDriverException:
            self.fail()

    def test_6_19_existingAccountConfirmation(self):
        try:
            self.driver.find_element_by_id("next-button").click()
            time.sleep(20)
            self.driver.find_element_by_xpath("//p-checkbox[@name='confirmedAccountCheckbox']").click()
            self.driver.find_element_by_id("next-button").click()
            self.driver.find_element_by_id("customer-finish-button").click()
            self.assertTrue(self.driver.find_element_by_xpath("//h3[contains(.,'Installation Complete')]"))
        except WebDriverException:
            self.fail()
