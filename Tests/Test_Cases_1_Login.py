import unittest
import os
from selenium.common.exceptions import WebDriverException
from Tests.Utility import Utility
import time
from selenium.webdriver.common.by import By


class Tests_Login(unittest.TestCase, Utility):

    ##############################
    # Before and after tests
    ###############

    @classmethod
    def setUpClass(self):
        self.setUpActions(self, withLogin=False)

    @classmethod
    def tearDownClass(self):
        self.tearDownActions(self)

    ##############################
    # Test cases
    ###############

    #####
    # Verifying the correct error messages are displayed for wrong credentials
    def test_1_01_wrongLogin(self):
        try:
            # checking for invalid credentials
            self.driver.find_element_by_id("username").send_keys("wrongUsername")
            self.driver.find_element_by_id("password").send_keys("")
            self.driver.find_element_by_id("login-button").click()
            time.sleep(1.5)
            self.assertFalse(self.is_element_present(By.XPATH, "//*[contains(text(),'Invalid login')]"),
                             "The error message could not be found")
        except WebDriverException:
            self.fail()



    #####
    # Testing the forgot password page - Does not test an actual flow to reset the password
    def test_1_02_forgotPassword(self):
        try:
            # navigate to the forgot password page
            self.driver.find_element_by_link_text("Forgot your password?").click()
            self.assertEqual(self.driver.current_url, self.backUrl + "forgot-password")

            # checking for invalid username
            self.driver.find_element_by_xpath("//*[contains(@class, 'forgot-password')]/div/input[@id='username']").send_keys('wrongUsername')
            self.driver.find_element_by_id("forgot-password-button").click()
            self.assertTrue(self.is_element_present(By.XPATH, "//*[contains(text(),'Invalid login')]"),
                            "The wrong login error message for forgotten password could not be found")
        except WebDriverException:
            self.fail()

    #####
    # Login with the right credentials
    def test_1_03_login(self):
        try:
            # Making sure the test starts on the right page
            if self.driver.current_url != self.backUrl + "login":
                self.driver.get(self.backUrl + "login")

            # Submitting a valid form
            self.driver.find_element_by_id("username").send_keys(self.username)
            self.driver.find_element_by_id("password").send_keys(self.password)
            self.driver.find_element_by_id("login-button").click()

            # Verify that the user is on the homepage, meaning he's logged in
            self.assertTrue(self.wait_for_url(self.backUrl + "home"), "The user couldn't sign in")
        except WebDriverException:
            self.fail()

    #####
    # Verify that the user is logged out by trying to go back to the Homepage while signed out
    def test_1_04_logOut(self):
        try:
            self.driver.find_element_by_link_text("sign out").click()
            self.wait_for_url(self.backUrl + "login")
            self.driver.get(self.backUrl + "home")
            self.assertTrue(self.wait_for_url(self.backUrl + "login"),
                            "The user was not redirected to the login page while he's supposed to be signed out")
        except WebDriverException:
            self.fail()

    #####
    # After login with valid credentials copy the url and paste in another browser(It should not display the user's welcome page)
    def test_1_05_copyURL(self):
        try:
            # Making sure test starts on the right page
            if self.driver.current_url != self.backUrl + "login":
                self.driver.get(self.backUrl + "login")

            # Submitting a valid form
            self.driver.find_element_by_id("username").send_keys(self.username)
            self.driver.find_element_by_id("password").send_keys(self.password)
            self.driver.find_element_by_id("login-button").click()

            # Verify that the user is on the homepage, meaning he's logged in
            self.assertTrue(self.wait_for_url(self.backUrl + "home"), "The user couldn't sign in")

            # Copy URL and paste on another browser
            # editing the code for switching browser(chrome to anyone(IE,firefox etc.))

        except WebDriverException:
            self.fail()

    #####
    # Login with NULL value in password field
    def test_1_06_unsuccessfulLogin(self):
        try:
                    # checking for invalid credentials
                    self.driver.find_element_by_link_text("sign out").click()
                    self.wait_for_url(self.backUrl + "login")
                    self.driver.find_element_by_id("username").send_keys("eng.directed123")
                    self.driver.find_element_by_id("password").send_keys("")
                    self.driver.find_element_by_id("login-button").click()
                    time.sleep(1.5)
                    self.assertFalse(self.is_element_present(By.XPATH, "//*[contains(text(),'Invalid login')]"),
                    "The error message could not be found(error: Invalid login credentials detected, please try again.)")

        except WebDriverException:
                    self.fail()








