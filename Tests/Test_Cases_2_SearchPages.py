import unittest
import time
import os
from selenium.common.exceptions import WebDriverException
from Tests.Utility import Utility
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import TimeoutException


class Tests_SearchPages(unittest.TestCase, Utility):

    ##############################
    # Before and after tests
    ###############

    @classmethod
    def setUpClass(inst):
        inst.setUpActions(inst)

    @classmethod
    def tearDownClass(inst):
        inst.tearDownActions(inst)

    ##############################
    # Saved Variables
    ###############
    _accountID = 'None'

    ##############################
    # Methods
    ###############

    #####
    # Verify that the button for the input works or not, depending on the number of chars
    def searchStringInput(self, tabNum):
        try:
            tabNum = str(tabNum)
            searchInput = self.driver.find_element_by_xpath("(//*[@id='searchInput'])[" + tabNum + "]")
            searchButton = self.driver.find_element_by_xpath("(//*[@id='searchButton'])[" + tabNum + "]")

            # Verify the button is disabled when only 2 chars
            searchInput.clear()
            searchInput.send_keys("12")
            self.assertFalse(searchButton.is_enabled())

            # Verify the button is enabled when more than 2 chars
            searchInput.send_keys("3")
            self.assertTrue(searchButton.is_enabled())
        except WebDriverException:
            self.fail()

    # Returns the number of results for the search
    def searchTabList(self, tabNum, searchWhat):
        try:
            tabNum = str(tabNum)
            searchInput = self.driver.find_element_by_xpath("(//*[@id='searchInput'])[" + tabNum + "]")
            searchButton = self.driver.find_element_by_xpath("(//*[@id='searchButton'])[" + tabNum + "]")

            # If the input needs to change value
            if searchWhat != False:
                searchInput.clear()
                searchInput.send_keys(searchWhat)

                # Waiting until the call is made. The button is enabled back when the loading is done
            searchButton.click()
            try:
                Wait(self.driver, 20).until(
                    EC.element_to_be_clickable((By.XPATH, "(//*[@id='searchButton'])[" + tabNum + "]")))
            except TimeoutException:
                self.fail("Result were not returned on time")
        except WebDriverException:
            self.fail()

    ##############################
    # Test cases
    ###############

    #####
    # Navigate to the Search page from the side menu
    def test_2_01_navigateToSearch(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.assertEqual(self.driver.current_url, self.backUrl + "search/list")
        except WebDriverException:
            self.fail()

    #####
    # ACCOUNTS - Verify that the button for the input works or not, depending on the number of chars
    def test_2_02_searchAccountStringInput(self):
        try:
            self.driver.find_element_by_xpath("//*[@id='deviceTab']/li[1]").click()
            time.sleep(0.2)
            self.searchStringInput(1)
        except WebDriverException:
            self.fail()

    #####
    # Verify the no result message on invalid accounts
    def test_2_03_searchAccountNoResult(self):
        try:
            self.searchTabList(1, "wrongAccount")
            self.assertTrue(self.driver.find_element_by_xpath("(//tbody)[1]/tr[1]").text, "No records found")
        except WebDriverException:
            self.fail()

    #####  
    # Making sure the searched value is present in the result list
    def test_2_04_searchAccountResults(self):
        try:
            self.searchTabList(1, self.accountEmail)
            self.assertTrue(self.is_element_present(By.XPATH, "(//tbody)[1]//td[contains(.,'" + self.accountEmail + "')]"))
        except WebDriverException:
            self.fail()

    #####
    # Verify that clicking on the ID of the row redirects on the corresponding account page
    def test_2_05_searchAccountGoToProfileWithID(self):
        try:
            # Verify that the account profile can be accessed by clicking on the accound ID
            elementAccountID = self.driver.find_element_by_xpath(
                "(//tbody)[1]//td[contains(.,'" + self.accountEmail + "')]/preceding-sibling::td[1]/span[2]")
            Tests_SearchPages._accountID = elementAccountID.text
            elementAccountID.click()
            self.assertEqual(self.driver.current_url, self.backUrl + "accounts/" + Tests_SearchPages._accountID)
        except WebDriverException:
            self.fail()

    #####
    # Verify that double clicking row redirects on the corresponding account page
    def test_2_06_searchAccountGoToProfileWithDblClick(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.searchTabList(1, self.accountEmail)

            ActionChains(self.driver).double_click(
                self.driver.find_element_by_xpath("(//tbody)[1]//td[contains(.,'" + self.accountEmail + "')]")).perform()
            self.assertEqual(self.driver.current_url, self.backUrl + "accounts/" + Tests_SearchPages._accountID)
        except WebDriverException:
            self.fail()

    #####
    # Verify that clicking on the ID of the row redirects on the corresponding account page
    def test_2_07_searchAccountGoToProfileWithEdit(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.searchTabList(1, self.accountEmail)
            self.driver.find_element_by_class_name("fa-pencil-square-o").click()
            self.assertEqual(self.driver.current_url, self.backUrl + "accounts/" + Tests_SearchPages._accountID)
        except WebDriverException:
            self.fail()

    #####
    # Verifying the pagination navigation of the list
    def test_2_08_searchAccountNavigatePages(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.searchTabList(1, "test")
            self.assertGreater(len(self.driver.find_elements_by_class_name("ui-paginator-page")), 1)

            self.driver.find_element_by_xpath("(//*[contains(@class,'fa-forward')])[1]").click()
            self.assertTrue(
                self.is_element_present(By.XPATH, "(//a[contains(@class,'ui-state-active') and text()='2'])[1]"))

            self.driver.find_element_by_xpath("//a[contains(@class,'ui-paginator-page') and text()='3'][1]").click()
            self.assertTrue(
                self.is_element_present(By.XPATH, "(//a[contains(@class,'ui-state-active') and text()='3'])[1]"))

            self.driver.find_element_by_xpath("(//*[contains(@class,'fa-step-backward')])[1]").click()
            self.assertTrue(
                self.is_element_present(By.XPATH, "(//a[contains(@class,'ui-state-active') and text()='1'])[1]"))
        except WebDriverException:
            self.fail()

    #####
    # Verifying that the sort of the columns is correct
    def test_2_09_searchAccountSort(self):
        try:
            SortColumn = self.driver.find_element_by_xpath("//th[contains(.,'Account Id')]")
            SortColumn.click()

            # converting the strings into ID to check if they are sorted correctly
            listToSort = self.driver.find_elements_by_xpath("(//tbody)[1]/tr/td[1]/span/a")
            for i, iValue in enumerate(listToSort):
                listToSort[i] = int(iValue.text)

            self.assertTrue(sorted(listToSort) == listToSort)

            SortColumn.click()
            listToSort = self.driver.find_elements_by_xpath("(//tbody)[1]/tr/td[1]/span/a")
            for i, iValue in enumerate(listToSort):
                listToSort[i] = int(iValue.text)
            self.assertTrue(sorted(listToSort, reverse=True) == listToSort)
        except WebDriverException:
            self.fail()

    #####
    # DEVICES - Verify that the button for the input works or not, depending on the number of chars
    def test_2_10_searchDeviceStringInput(self):
        try:
            self.driver.find_element_by_xpath("//*[@id='deviceTab']/li[2]").click()
            time.sleep(0.2)
            self.searchStringInput(2)
        except WebDriverException:
            self.fail()

    #####
    # Verify the no result message on invalid devices
    def test_2_11_searchDeviceNoResult(self):
        try:
            self.searchTabList(2, "wrongDevice")
            self.assertTrue(self.driver.find_element_by_xpath("(//tbody)[2]/tr[1]").text, "No records found")
        except WebDriverException:
            self.fail()

    #####  
    # Making sure the searched value is present in the result list
    def test_2_12_searchDeviceResults(self):
        try:
            self.searchTabList(2, self.activeDeviceID)
            self.assertTrue(
                self.is_element_present(By.XPATH, "(//tbody)[2]//td[contains(.,'" + self.activeDeviceID + "')]"))
        except WebDriverException:
            self.fail()

    #####
    # Verify the status search is correct
    def test_2_13_searchDeviceStatus(self):
        try:
            # Verify that options are displayed
            self.driver.find_element_by_class_name("ui-dropdown-label").click()
            self.assertTrue(self.is_element_present(By.CLASS_NAME, 'ui-dropdown-list'))

            # Search with a status that returns results
            chosenStatus = 'ActiveWithAsset'
            self.driver.find_element_by_xpath(
                "//ul[contains(@class,'ui-dropdown-list')]/li/span[contains(.,'" + chosenStatus + "')]").click()
            self.searchTabList(2, False)
            statusList = self.driver.find_elements_by_xpath("(//tbody)[2]//td[4]")
            if len(statusList) == 0: self.fail("No results were found. Therefore this case cannot be testing correctly")

            # If ALL status in the list matches the search, it is correct
            for status in statusList:
                self.assertTrue(status.text == chosenStatus)
        except WebDriverException:
            self.fail()

    #####
    # Verify that clicking on the ID of the row redirects on the corresponding device page
    def test_2_14_searchDeviceGoToDetailsWithID(self):
        try:
            self.driver.find_element_by_class_name("ui-dropdown-label").click()
            self.driver.find_element_by_xpath(
                "//ul[contains(@class,'ui-dropdown-list')]/li/span[contains(.,'All')]").click()
            self.searchTabList(2, False)

            # Verify that the device details can be accessed by clicking on the Air ID
            self.driver.find_element_by_xpath("(//tbody)[2]//td[contains(.,'" + self.activeDeviceID + "')]").click()
            self.assertTrue(self.backUrl + "devices/" in self.driver.current_url)
            # TODO: Need better assert
        except WebDriverException:
            self.fail()

    #####
    # Verify that clicking on the account ID redirects to the corresponding account
    def test_2_15_searchDeviceGoToDetailsWithAccount(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.driver.find_element_by_xpath("//*[@id='deviceTab']/li[2]").click()
            time.sleep(0.2)

            # verify that the device details can be accessed by double clicking on the row
            self.searchTabList(2, self.activeDeviceID)

            self.driver.find_element_by_xpath(
                "(//tbody)[2]//td[contains(.,'" + self.activeDeviceID + "')]/following-sibling::td[1]/span[2]").click()
            self.assertTrue(self.backUrl + "accounts/" in self.driver.current_url)
            # TODO: Need better assert
        except WebDriverException:
            self.fail()

    #####
    # Verify that double clicking row redirects on the corresponding account page
    def test_2_16_searchDeviceGoToDetailsWithDblClick(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.driver.find_element_by_xpath("//*[@id='deviceTab']/li[2]").click()
            time.sleep(0.2)

            # verify that the device details can be accessed by double clicking on the row
            self.searchTabList(2, self.activeDeviceID)

            ActionChains(self.driver).double_click(
                self.driver.find_element_by_xpath("(//tbody)[2]//td[contains(.,'" + self.activeDeviceID + "')]")).perform()
            self.assertTrue(self.backUrl + "devices/" in self.driver.current_url)
            # TODO: Need better assert
        except WebDriverException:
            self.fail()

    #####
    # Verify that the device details can be accessed by clicking on the edit icon
    def test_2_17_searchDeviceGoToDetailsWithEdit(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.driver.find_element_by_xpath("//*[@id='deviceTab']/li[2]").click()
            time.sleep(0.2)

            self.searchTabList(2, self.activeDeviceID)

            self.driver.find_element_by_class_name("fa-pencil-square-o").click()
            self.assertTrue(self.backUrl + "devices/" in self.driver.current_url)
            # TODO: Need better assert
        except WebDriverException:
            self.fail()

    #####
    # Verifying the pagination navigation of the list
    def test_2_18_searchDeviceNavigatePages(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.driver.find_element_by_xpath("//*[@id='deviceTab']/li[2]").click()
            time.sleep(0.2)
            self.searchTabList(2, "test")
            self.assertGreater(len(self.driver.find_elements_by_class_name("ui-paginator-page")), 1)

            self.driver.find_element_by_xpath("(//*[contains(@class,'fa-forward')])[2]").click()
            self.assertTrue(self.is_element_present(By.XPATH, "//a[contains(@class,'ui-state-active') and text()='2']"))

            self.driver.find_element_by_xpath("//a[contains(@class,'ui-paginator-page') and text()='3']").click()
            self.assertTrue(self.is_element_present(By.XPATH, "//a[contains(@class,'ui-state-active') and text()='3']"))

            self.driver.find_element_by_xpath("(//*[contains(@class,'fa-step-backward')])[2]").click()
            self.assertTrue(
                self.is_element_present(By.XPATH, "(//a[contains(@class,'ui-state-active') and text()='1'])[2]"))
        except WebDriverException:
            self.fail()

    #####
    # Verifying that the sort of the columns is correct
    def test_2_19_searchDeviceSort(self):
        try:
            SortColumn = self.driver.find_element_by_xpath("//th[contains(.,'Air ID')]")
            SortColumn.click()
            # converting the strings into ID to check if they are sorted correctly
            listToSort = self.driver.find_elements_by_xpath("(//tbody)[2]/tr/td[1]/span/a")
            for i, iValue in enumerate(listToSort):
                listToSort[i] = iValue.text
            self.assertTrue(sorted(listToSort, key=lambda s: s.lower()) == listToSort)

            SortColumn.click()
            listToSort = self.driver.find_elements_by_xpath("(//tbody)[2]/tr/td[1]/span/a")
            for i, iValue in enumerate(listToSort):
                listToSort[i] = iValue.text
            self.assertTrue(sorted(listToSort, reverse=True, key=lambda s: s.lower()) == listToSort)
        except WebDriverException:
            self.fail()

    #####
    # USERS - Verify that the button for the input works or not, depending on the number of chars
    def test_2_20_searchUserStringInput(self):
        try:
            self.driver.find_element_by_xpath("//*[@id='deviceTab']/li[3]").click()
            time.sleep(0.2)
            self.searchStringInput(3)
        except WebDriverException:
            self.fail()

    #####
    # Verify the no result message on invalid devices
    def test_2_21_searchUserNoResult(self):
        try:
            self.searchTabList(3, "wrongUser")
            self.assertTrue(self.driver.find_element_by_xpath("(//tbody)[3]/tr[1]").text, "No records found")
        except WebDriverException:
            self.fail()

    #####  
    # Making sure the searched value is present in the result list
    def test_2_22_searchUserResults(self):
        try:
            self.searchTabList(3, self.testUser)
            self.assertTrue(self.is_element_present(By.XPATH, "(//tbody)[3]//td[contains(.,'" + self.testUser + "')]"))
        except WebDriverException:
            self.fail()

    #####
    # Verify that clicking on the ID of the row redirects on the corresponding user page
    def test_2_23_searchUserGoToDetailsWithID(self):
        try:
            # Verify that the user page can be accessed by clicking on the Air ID
            self.driver.find_element_by_xpath("(//tbody)[3]//td[contains(.,'" + self.testUser + "')]/span[2]").click()
            self.assertTrue(self.backUrl + "users/" in self.driver.current_url)
            # TODO: Need better assert
        except WebDriverException:
            self.fail()

    #####
    # Verify that clicking on the account ID redirects to the corresponding account page
    def test_2_24_searchUserGoToDetailsWithAccount(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.driver.find_element_by_xpath("//*[@id='deviceTab']/li[3]").click()
            time.sleep(0.2)

            self.searchTabList(3, self.testUser)
            elementAccountID = self.driver.find_element_by_xpath(
                "(//tbody)[3]//td[contains(.,'" + self.testUser + "')]/following-sibling::td[3]/span[2]")
            Tests_SearchPages._accountID = elementAccountID.text
            elementAccountID.click()
            self.assertEqual(self.driver.current_url, self.backUrl + "accounts/" + Tests_SearchPages._accountID)
        except WebDriverException:
            self.fail()

    #####
    # Verify that double clicking row redirects on the corresponding user page
    def test_2_25_searchUserGoToDetailsWithDblClick(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.driver.find_element_by_xpath("//*[@id='deviceTab']/li[3]").click()
            time.sleep(0.2)

            # verify that the user page can be accessed by double clicking on the row
            self.searchTabList(3, self.testUser)

            ActionChains(self.driver).double_click(
                self.driver.find_element_by_xpath("(//tbody)[3]//td[contains(.,'" + self.testUser + "')]")).perform()
            self.assertTrue(self.backUrl + "users/" in self.driver.current_url)
            # TODO: Need better assert
        except WebDriverException:
            self.fail()

    #####
    # Verify that the device details can be accessed by clicking on the edit icon
    def test_2_26_searchUserGoToDetailsWithEdit(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.driver.find_element_by_xpath("//*[@id='deviceTab']/li[3]").click()
            time.sleep(0.2)

            # verify that the user page can be accessed by clicking on the edit icon
            self.searchTabList(3, self.testUser)

            self.driver.find_element_by_class_name("fa-pencil-square-o").click()
            self.assertTrue(self.backUrl + "users/" in self.driver.current_url)
            # TODO: Need better assert
        except WebDriverException:
            self.fail()

    #####
    # Verify that the delete button is working correctly (NOT DELETING A USER)
    def test_2_27_searchUserDeletebButton(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.driver.find_element_by_xpath("//*[@id='deviceTab']/li[3]").click()
            time.sleep(0.2)

            self.searchTabList(3, self.testUser)
            self.driver.find_element_by_class_name("fa-trash-o").click()
            self.assertTrue(self.is_element_present(By.XPATH, "//span[contains(.,'Delete Confirmation')]"))
            self.driver.find_element_by_xpath(
                "//*[contains(@class,'ui-dialog-footer')]/button/span[contains(.,'No')]").click()
            try:
                Wait(self.driver, 5).until(
                    EC.invisibility_of_element_located((By.XPATH, "//span[contains(.,'Delete Confirmation')]")))
            except TimeoutException:
                self.fail("The confirmation popup didn't disappear")
        except WebDriverException:
            self.fail()

    #####
    # Verifying the pagination navigation of the list
    def test_2_28_searchDeviceNavigatePages(self):
        try:
            self.driver.find_element_by_link_text("Search").click()
            self.driver.find_element_by_xpath("//*[@id='deviceTab']/li[3]").click()
            time.sleep(0.2)
            self.searchTabList(3, self.testUser30)
            self.assertGreater(len(self.driver.find_elements_by_class_name("ui-paginator-page")), 1)

            self.driver.find_element_by_xpath("(//*[contains(@class,'fa-forward')])[3]").click()
            self.assertTrue(self.is_element_present(By.XPATH, "//a[contains(@class,'ui-state-active') and text()='2']"))

            self.driver.find_element_by_xpath("//a[contains(@class,'ui-paginator-page') and text()='3']").click()
            self.assertTrue(self.is_element_present(By.XPATH, "//a[contains(@class,'ui-state-active') and text()='3']"))

            self.driver.find_element_by_xpath("(//*[contains(@class,'fa-step-backward')])[3]").click()
            self.assertTrue(
                self.is_element_present(By.XPATH, "(//a[contains(@class,'ui-state-active') and text()='1'])[3]"))
        except WebDriverException:
            self.fail()

    #####
    # Verifying that the sort of the columns is correct
    def test_2_29_searchDeviceSort(self):
        try:
            SortColumn = self.driver.find_element_by_xpath("//th[contains(.,'User Name')]")
            SortColumn.click()

            # converting the strings into ID to check if they are sorted correctly
            listToSort = self.driver.find_elements_by_xpath("(//tbody)[3]/tr/td[1]/span/a")
            for i, iValue in enumerate(listToSort):
                listToSort[i] = iValue.text
            self.assertTrue(sorted(listToSort, key=lambda s: s.lower()) == listToSort)

            SortColumn.click()
            listToSort = self.driver.find_elements_by_xpath("(//tbody)[3]/tr/td[1]/span/a")
            for i, iValue in enumerate(listToSort):
                listToSort[i] = iValue.text
            self.assertTrue(sorted(listToSort, reverse=True, key=lambda s: s.lower()) == listToSort)
        except WebDriverException:
            self.fail()
