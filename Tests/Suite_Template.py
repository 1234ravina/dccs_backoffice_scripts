""" 
 Using this template as a base for creating new suites
 Just copy-paste this begin scripting!
"""

import unittest
import time
import Tests.Utility as Utility
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

class Tests_Suite_Template(unittest.TestCase, Utility):
    
##############################
# Before and after tests
###############

    @classmethod
    def setUpClass(inst): inst.setUpActions(inst)
                
    @classmethod
    def tearDownClass(inst): inst.tearDownActions(inst)

##############################
# Test cases
###############
    def test_2_01(self):
        self.assertTrue(True)

    def test_2_02(self):
        self.fail("I am a disapointment");