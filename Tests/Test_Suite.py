import unittest
import os
import Tests.HTMLTestRunner as HTMLTestRunner
import time
import datetime
import shutil
from Tests.Test_Cases_1_Login import Tests_Login
from Tests.Test_Cases_2_SearchPages import Tests_SearchPages
from Tests.Test_Cases_3_Account import Tests_Account
from Tests.Test_Cases_4_Devices import Tests_Devices 
from Tests.Test_Cases_6_InstallDevice import Tests_InstallDevice
from Tests.Test_Cases_5_Users import Tests_Users
from Tests.Test_Cases_7_DefectiveDevice import Tests_DefectiveDevices

#####
# Load all the test suites
tc1 = unittest.TestLoader().loadTestsFromTestCase(Tests_Login)
tc2 = unittest.TestLoader().loadTestsFromTestCase(Tests_SearchPages)
tc3 = unittest.TestLoader().loadTestsFromTestCase(Tests_Account)
tc4 = unittest.TestLoader().loadTestsFromTestCase(Tests_Devices)
tc5 = unittest.TestLoader().loadTestsFromTestCase(Tests_Users)
tc6 = unittest.TestLoader().loadTestsFromTestCase(Tests_InstallDevice)
tc7 = unittest.TestLoader().loadTestsFromTestCase(Tests_DefectiveDevices)

#####
# Choose which suites you want to run
test_suite = unittest.TestSuite([tc7])
partial_suite = unittest.TestSuite([tc4])
#test_suite = unittest.TestSuite([t_back, t_front])

#Generate a timestamp to name the test_output report to not overwrite the last one
ts = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d_%H%M')
outfile = open(os.getcwd() + "/test_outputs/Report_" + ts + ".html", "w")
outfolder = os.getcwd() + "/test_outputs/"
runner = HTMLTestRunner.HTMLTestRunner(stream=outfile, title="Test Report")

print("running the test suite...")

"""===============================================================================
=== Run this line to get full prints of the scripts (but no reports, used mostly for development)""" 
#runner.run = unittest.TextTestRunner(verbosity=2).run(partial_suite)

"""===============================================================================
=== Run this line to generate the report at the end of the run (doesn't print what are in the suites classes) """
runner.run(test_suite)
