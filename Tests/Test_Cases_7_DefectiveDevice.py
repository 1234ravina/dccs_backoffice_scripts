import unittest
import os
from selenium.common.exceptions import WebDriverException
from Tests.Utility import Utility
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


class Tests_DefectiveDevices(unittest.TestCase, Utility):

    ##############################
    # Before and after tests
    ###############

    @classmethod
    def setUpClass(inst): inst.setUpActions(inst)

    @classmethod
    def tearDownClass(inst): inst.tearDownActions(inst)

    ##############################
    # Test cases
    ###############
    def test_7_01(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Admin')]").click()
            self.driver.find_element_by_xpath("//a[contains(.,'Defective Devices')]").click()
            deviceSearch = self.driver.find_element_by_xpath("//app-device-search//input")
            deviceSearch.send_keys(self.defectiveDeviceID[:2])
            searchInput = self.driver.find_element_by_class_name("ui-autocomplete-panel")
            addButton = self.driver.find_element_by_xpath("//app-device-search/following-sibling::button")

            self.assertFalse(addButton.is_enabled())
            self.assertFalse(searchInput.is_displayed())

            deviceSearch.send_keys(self.defectiveDeviceID[2])
            self.assertTrue(self.is_element_present(By.CLASS_NAME, "ui-autocomplete-panel"))
            self.driver.find_element_by_xpath(
                "//*[contains(@class,'ui-autocomplete-panel')]//li[contains(.,'" + self.defectiveDeviceID + "')]").click()
            addButton.click()
            Wait(self.driver, 30).until(
                EC.presence_of_element_located((By.XPATH, "//span[contains(.,'" + self.defectiveDeviceID + "')]")))
        except WebDriverException:
            self.fail()

    def test_7_02(self):
        try:
            self.driver.find_element_by_xpath("//app-defective-device//@title[contains(.,'Delete')]").click()
            self.driver.find_element_by_xpath("//app-defective-device/p-confirmdialog//button[contains(.,'No')]").click()
            self.assertTrue(self.is_element_present(By.XPATH, "//span[contains(.,'" + self.defectiveDeviceID + "')]"))
            self.driver.find_element_by_xpath("//app-defective-device//@title[contains(.,'Delete')]").click()
            self.driver.find_element_by_xpath("//app-defective-device/p-confirmdialog//button[contains(.,'Yes')]").click()
        except WebDriverException:
            self.fail()

    def test_7_03(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Admin')]").click()
            self.driver.find_element_by_xpath("//a[contains(.,'Upgrade Offer')]").click()
            self.driver.find_element_by_id("upgradeOfferCreate").click()
            self.driver.find_element_by_id("data-field").send_keys("ZRT")
            self.driver.find_element_by_xpath(
                "//app-upgrade-offer-create//p-dropdown//label[contains(.,'Select Program')]").click()
            self.driver.find_element_by_xpath(
                "//app-upgrade-offer-create//p-dropdown//descendant::span[contains(.,'3G')]").click()
            self.driver.find_element_by_xpath(
                "//app-upgrade-offer-create//p-dropdown//label[contains(.,'Select Offer')]").click()
            self.driver.find_element_by_xpath(
                "//app-upgrade-offer-create//p-dropdown//descendant::span[contains(.,'Offer 2')]").click()
            self.driver.find_element_by_xpath(
                "//app-upgrade-offer-create//p-calendar//span//@name[contains(.,'startDate')]").send_keys(
                "2018-09-01 12:00")
            self.driver.find_element_by_id("createButton").click()
            self.driver.find_element_by_xpath("//*[@id=\"upgradeOfferTab\"]//li[contains(.,'Search')]").click()
            self.driver.find_element_by_id("searchInput").send_keys("ZRT")
        except WebDriverException:
            self.fail()
