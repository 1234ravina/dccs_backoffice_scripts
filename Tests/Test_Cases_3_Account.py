import unittest
import time
import os
from Tests.Utility import Utility
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import ElementClickInterceptedException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


class Tests_Account(unittest.TestCase, Utility):

    ##############################
    # Before and after tests
    ###############

    @classmethod
    def setUpClass(inst):
        inst.setUpActions(inst)

    @classmethod
    def tearDownClass(inst):
        inst.tearDownActions(inst)

    ##############################
    # Test cases
    ###############

    #####
    # Navigate to the Search page from the side menu
    def test_3_01_navigateToAccount(self):
        try:
            self.driver.find_element_by_link_text("Accounts").click()
            self.assertTrue(self.backUrl + "accounts/list" in self.driver.current_url)
        except WebDriverException:
            self.fail()

    #####
    # Verify the autocomplete input is correct
    def test_3_02_accountLookup(self):
        try:
            accountSearch = self.driver.find_element_by_xpath("//app-account-search//input")
            accountSearch.send_keys("12")
            searchInput = self.driver.find_element_by_class_name("ui-autocomplete-panel")
            searchButton = self.driver.find_element_by_xpath("//app-account-search/following-sibling::button")

            self.assertFalse(searchButton.is_enabled())
            self.assertFalse(searchInput.is_displayed())

            accountSearch.send_keys("3")
            self.assertTrue(self.is_element_present(By.CLASS_NAME, "ui-autocomplete-panel"))
            self.driver.find_element_by_xpath("//*[contains(@class,'ui-autocomplete-panel')]//li[1]").click()
            searchButton.click()
            self.assertTrue(self.backUrl + "accounts/" in self.driver.current_url)
            # TODO: Needs better assert
        except WebDriverException:
            self.fail()

    #####12
    # Verify the multiple back buttons
    def test_3_03_navigateAccountPage(self):
        try:
            self.driver.find_element_by_id("cancelButton").click()
            self.assertTrue(self.backUrl + "accounts/list" in self.driver.current_url)
            self.driver.back()

            self.driver.find_element_by_xpath("//button/span[contains(.,'Return to Search')]").click()
            self.assertTrue(self.backUrl + "accounts/list" in self.driver.current_url)
            self.driver.back()

            # Wait(self.driver,5).until(EC.presence_of_element_located((By.XPATH,"")))
            accountID = self.driver.find_element_by_xpath(
                "//label[contains(.,'Account ID')]/following-sibling::input").get_attribute('value')
            accountUsername = self.driver.find_element_by_xpath(
                "//label[contains(.,'Account Name')]/following-sibling::input").get_attribute('value')
            self.driver.find_element_by_id('addUserToAccount').click()
            time.sleep(3)
            self.assertEquals(self.driver.current_url,
                              self.backUrl + "users?accountId=" + accountID + "&username=" + accountUsername)
        except WebDriverException:
            self.fail()

    #####
    # Navigate to the Create Account page and coming back
    def test_3_04_navigateToCreateAccount(self):
        try:
            self.driver.find_element_by_link_text("Accounts").click()
            self.driver.find_element_by_id("createButton").click()
            self.assertEqual(self.driver.current_url, self.backUrl + "accounts/create")
            self.driver.find_element_by_id("cancelButton").click()
            self.assertTrue(self.backUrl + "accounts/list" in self.driver.current_url)
        except WebDriverException:
            self.fail()

    #####
    # Create a new account
    def test_3_05_createAccount(self):
        try:
            # Navigate to the creation page
            self.driver.find_element_by_id("createButton").click()
            self.assertFalse(self.driver.find_element_by_id("createButton").is_enabled())

            # Enter the name of the account and validating its creation
            nameInput = self.driver.find_element_by_xpath("//label[contains(.,'Account Name')]/following-sibling::input")
            nameInput.send_keys(self.newUserAccountName)
            self.driver.find_element_by_id("createButton").click()
            Wait(self.driver, 30).until_not(EC.url_matches(self.backUrl + "accounts/create"))
            time.sleep(3)

            # Verifying the values and storing some
            accountName = self.driver.find_element_by_xpath(
                "//label[contains(.,'Account Name')]/following-sibling::input").get_attribute('value')
            self.__class__.newUserAccountId = self.driver.find_element_by_xpath(
                "//label[contains(.,'Account ID')]/following-sibling::input").get_attribute('value')
            self.assertEqual(accountName, self.newUserAccountName)
        except WebDriverException:
            self.fail()

    #####
    # Verify the different behaviors of editing wrongly the information of the account
    def test_3_06_editErrorMessagesAccount(self):
        try:
            firstNameInput = self.driver.find_element_by_xpath("//label[contains(.,'First Name')]/following-sibling::input")
            self.assertFalse(self.driver.find_element_by_id("updateAddressButton").is_enabled())

            emailInput = self.driver.find_element_by_xpath("//label[contains(.,'Contact Email')]/following-sibling::input")
            emailInput.send_keys("wrongEmailFormat" + Keys.ENTER)
            self.assertTrue(self.driver.find_element_by_xpath("//div[@role='alert'][contains(.,'email')]").is_displayed())

            firstNameInput.send_keys(Keys.ENTER)
            self.assertTrue(
                self.driver.find_element_by_xpath("//div[@role='alert'][contains(.,'required')]").is_displayed())
        except WebDriverException:
            self.fail()

    #####  
    # 
    def test_3_07_editAccount(self):
        try:
            updateButton = self.driver.find_element_by_id("updateAddressButton")
            self.assertFalse(updateButton.is_enabled())

            # All the mandatory fields that needs to be set
            emailInput = self.driver.find_element_by_xpath("//label[contains(.,'Contact Email')]/following-sibling::input")
            firstNameInput = self.driver.find_element_by_xpath("//label[contains(.,'First Name')]/following-sibling::input")
            lastNameInput = self.driver.find_element_by_xpath("//label[contains(.,'Last Name')]/following-sibling::input")
            homePhoneInput = self.driver.find_element_by_xpath("//label[contains(.,'Home Phone')]/following-sibling::input")
            addressInput = self.driver.find_element_by_xpath(
                "//label[contains(.,'Street Address')]/following-sibling::input")
            cityInput = self.driver.find_element_by_xpath("//label[contains(.,'City')]/following-sibling::input")
            stateDropdown = self.driver.find_element_by_xpath("//label[contains(.,'State')]/following-sibling::p-dropdown")
            zipCodeInput = self.driver.find_element_by_xpath("//label[contains(.,'Zip Code')]/following-sibling::input")

            # clearing these two as they were used above
            emailInput.clear()
            firstNameInput.clear()

            # Entering info for every fields
            emailInput.send_keys('mytestemail@email.com')
            firstNameInput.send_keys('Automation')
            lastNameInput.send_keys('Tester')
            homePhoneInput.send_keys('1231231234')
            addressInput.send_keys('123 first avenue')
            cityInput.send_keys('Springfield')
            stateDropdown.click()
            self.driver.find_element_by_xpath("//label[contains(.,'State')]/following-sibling::p-dropdown//li[1]").click()
            zipCodeInput.send_keys('45002')
            self.assertTrue(updateButton.is_enabled())

            # Verifying the update was successful
            updateButton.click()
            # self.driver.save_screenshot('sample.png')
            Wait(self.driver, 10).until(EC.element_to_be_clickable((By.ID, "updateAddressButton")))
            self.driver.refresh()
            Wait(self.driver, 10).until(EC.presence_of_element_located((By.ID, "updateAddressButton")))
            time.sleep(1)
            emailInput = self.driver.find_element_by_xpath("//label[contains(.,'Contact Email')]/following-sibling::input")
            self.assertEqual(emailInput.get_attribute('value'), 'mytestemail@email.com')
        except WebDriverException:
            self.fail()

    #####
    # Verify that the shipping tool opens in a new tab
    def test_3_08_shippingTool(self):
        try:
            time.sleep(5)
            email = self.driver.find_element_by_xpath(
                "//label[contains(.,'Account Name')]/following-sibling::input").get_attribute('value')
            self.driver.find_element_by_link_text('Go To Shipping Tool').click()
            self.driver.switch_to_window(self.driver.window_handles[1])
            self.assertEqual(self.driver.current_url, "http://shipments.directed.com/EmailSearch.aspx?Email=" + email)
            self.driver.switch_to_window(self.driver.window_handles[0])
        except WebDriverException:
            self.fail()

    #####
    # Verifying that the move account functionnality is working as intended
    def test_3_09_moveAccount(self):
        try:
            self.driver.find_element_by_link_text("Management").click()
            moveButton = self.driver.find_element_by_id('moveButton')
            nameInput = self.driver.find_element_by_xpath("//span[contains(.,'Account to Move To:')]//input")
            self.assertFalse(moveButton.is_enabled())

            # lookup with accountName
            nameInput.send_keys("12")
            self.assertFalse(self.driver.find_element_by_xpath(
                "//span[contains(.,'Account to Move To:')]//*[contains(@class,'ui-autocomplete-panel')]").is_displayed())
            nameInput.send_keys("3" + Keys.ENTER)
            try:
                Wait(self.driver, 10).until(EC.presence_of_element_located(
                    (By.XPATH, "//span[contains(.,'Account to Move To:')]//*[contains(@class,'ui-autocomplete-panel')]")))
            except TimeoutError:
                self.fail()
            nameInput.clear()
            self.assertFalse(moveButton.is_enabled())

            # lookup with accountID
            self.driver.find_element_by_xpath("//span[contains(.,'Account ID:')]//input").send_keys('45')
            self.assertTrue(moveButton.is_enabled())
            # moveButton.click()
            self.fail('Not a valid ID to move. Gives root account error')
        except WebDriverException:
            self.fail()

    #####  
    # 
    def test_3_10_disableAccount(self):
        try:
            self.driver.find_element_by_link_text("Details").click()
            self.driver.find_element_by_id("disableButton").click()
            self.assertTrue(self.driver.find_element_by_xpath("//span[contains(.,'Disable Confirmation')]").is_displayed())
            self.driver.find_element_by_xpath("//button[2]/span[2][contains(.,'No')]").click()
            self.assertFalse(self.driver.find_element_by_xpath("//span[contains(.,'Disable Confirmation')]").is_displayed())
        except WebDriverException:
            self.fail()

    #####   
    # Verify the cancelation of deleting the newly created account
    def test_3_11_deleteAccountCancel(self):
        try:
            self.driver.find_element_by_link_text("Details").click()
            Wait(self.driver, 20).until(EC.element_to_be_clickable((By.ID, "deleteButton")))
            self.driver.find_element_by_id("deleteButton").click()
            self.assertTrue(self.driver.find_element_by_xpath("//span[contains(.,'Delete Confirmation')]").is_displayed())
            self.driver.find_element_by_xpath("//button[2]/span[contains(.,'No')]").click()
            self.assertFalse(self.driver.find_element_by_xpath("//span[contains(.,'Delete Confirmation')]").is_displayed())

            # This is here because of a bug where there's an infinite loading after clicking on "No" to not delete the account
            '''    if self.driver.find_element_by_class_name("spinner").is_displayed is True:
                time.sleep(3)
                if self.driver.find_element_by_class_name("spinner").is_displayed is True:
                    self.driver.refresh()
                    print("Long loading detected.")
                    # self.fail("Long loading detected.")'''
        except WebDriverException:
            self.fail()

    #####   
    # Confirm deletion of the newly created account 
    def test_3_12_deleteAccountConfirm(self):
        try:
            # Make sure the URL is correct so the account is deleted even if previous test fails
            self.driver.get(self.backUrl + "accounts/" + self.__class__.newUserAccountId)

            self.driver.find_element_by_id("deleteButton").click()
            self.driver.find_element_by_xpath("//button[1]/span[contains(.,'Yes')]").click()

            Wait(self.driver, 30).until(EC.url_changes(self.driver.current_url))
            self.driver.find_element_by_xpath("//app-account-search//input").send_keys(self.newUserAccountName + Keys.ENTER)
            self.assertFalse(self.is_element_present(By.XPATH,
                                                     "//*[contains(@class,'ui-autocomplete-panel')]//li[contains(.,'" + self.__class__.newUserAccountName + "')]"))
        except WebDriverException:
            self.fail()
