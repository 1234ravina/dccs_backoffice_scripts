import unittest
import time
from Tests.Utility import Utility
import os
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


class Tests_Users(unittest.TestCase, Utility):

    ##############################
    # Before and after tests
    ###############

    @classmethod
    def setUpClass(inst):
        inst.setUpActions(inst)

    @classmethod
    def tearDownClass(inst):
        inst.tearDownActions(inst)

    ##############################
    # Test cases
    ###############

    #####
    # Navigate to the Users page from the side menu
    def test_5_01_navigateToUser(self):
        try:
            self.driver.find_element_by_link_text("Users").click()
            self.assertTrue(self.backUrl + "users/list" in self.driver.current_url)
        except WebDriverException:
            self.fail()

    #####
    # Navigate back an forth the create user page
    def test_5_02_navigateToCreateUser(self):
        try:
            self.driver.find_element_by_id('createButton').click()
            self.assertEquals(self.driver.current_url, self.backUrl + "users")

            self.driver.find_element_by_xpath("//button[contains(.,'Return to Search')]").click()
            self.assertEquals(self.driver.current_url, self.backUrl + "users/list")
            self.driver.find_element_by_id('createButton').click()
        except WebDriverException:
            self.fail()

    #####
    # By selecting an already existing email, the ID should be filled automatically
    def test_5_03_createUserFillAccountID(self):
        try:
            self.assertFalse(
            self.is_element_present(By.XPATH, "//label[contains(.,'Security Role')]/following-sibling::p-dropdown"))
            self.driver.find_element_by_xpath("//app-account-search//input").send_keys("test")

            self.assertTrue(self.is_element_present(By.CLASS_NAME, "ui-autocomplete-panel"))
            self.driver.find_element_by_xpath("//*[contains(@class,'ui-autocomplete-panel')]//li[1]").click()
            self.__class__.accountID = self.driver.find_element_by_xpath("//label[contains(.,'Account ID')]/following-sibling::input").get_attribute('value')
            self.assertTrue(self.__class__.accountID)
            self.assertTrue(
            self.is_element_present(By.XPATH, '//label[contains(.,"Security Role")]/following-sibling::p-dropdown'))
            self.assertFalse(self.driver.find_element_by_xpath('//button[contains(.,"Cancel")]').is_enabled())
        except WebDriverException:
            self.fail()

    #####
    # Verifying some error messages and the cancel
    def test_5_04_createUserErrors(self):
        try:
            self.driver.find_element_by_xpath("//label[contains(.,'Email')]/following-sibling::input").send_keys('test' + Keys.ENTER)
            self.assertTrue(self.is_element_present(By.XPATH, '//*[contains(.,"Invalid email address string.")]'))

            self.driver.find_element_by_xpath('//button[contains(.,"Cancel")]').click()
            emailValue = self.driver.find_element_by_xpath("//label[contains(.,'Account ID')]/following-sibling::input").get_attribute('value')
            self.assertFalse(emailValue.strip())
            self.assertFalse(self.driver.find_element_by_xpath("//b[contains(.,'Account Lookup:')]/following-sibling::p-autocomplete").get_attribute('value'))
        except WebDriverException:
            self.fail()

    #####   
    # Verifying that passwords can be generated
    def test_5_05_createUsersPassword(self):
        try:
            generatePwd = self.driver.find_element_by_xpath('//button[contains(.,"Generate")]')
            generatePwd.click()
            firstPwd = self.driver.find_element_by_id('generate-password-input').get_attribute('value')
            self.assertTrue(firstPwd)

            generatePwd.click()
            secondPwd = self.driver.find_element_by_id('generate-password-input').get_attribute('value')
            self.assertNotEqual(firstPwd, secondPwd)

            self.driver.find_element_by_xpath("//button[.='Use']").click()
            self.assertEqual(
            self.driver.find_element_by_xpath("//label[.='Password']/following-sibling::input").get_attribute('value'),secondPwd)
            self.assertEqual(
            self.driver.find_element_by_xpath("//label[.='Confirm Password']/following-sibling::input").get_attribute('value'), secondPwd)
        except WebDriverException:
            self.fail()

    #####   
    # 
    def test_5_06_createUserCompleteForm(self):
        try:
            accountID = self.driver.find_element_by_xpath("//label[.='Account ID']/following-sibling::input")
            username = self.driver.find_element_by_xpath("//label[.='Username']/following-sibling::input")
            email = self.driver.find_element_by_xpath("//label[.='Email']/following-sibling::input")
            firstName = self.driver.find_element_by_xpath("//label[.='First Name']/following-sibling::input")
            lastName = self.driver.find_element_by_xpath("//label[.='Last Name']/following-sibling::input")
            phone = self.driver.find_element_by_xpath("//label[.='Phone #']/following-sibling::input")

            accountID.clear()
            accountID.send_keys("3000000087")

            randomName = "testTester" + str(self.randomNum)
            username.send_keys(randomName)
            email.send_keys("testerengdirected" + str(self.randomNum) + "@yopmail.com")
            firstName.send_keys("Test")
            lastName.send_keys("Tester")
            phone.send_keys("1231231234")
            self.driver.find_element_by_xpath('//label[contains(.,"Security Role")]/following-sibling::p-dropdown').click()
            self.driver.find_element_by_xpath('//label[contains(.,"Security Role")]/following-sibling::p-dropdown//li[2]').click()
            self.driver.find_element_by_xpath('//label[contains(.,"External Type")]/following-sibling::p-dropdown').click()
            self.driver.find_element_by_xpath('//label[contains(.,"External Type")]/following-sibling::p-dropdown//li[2]').click()
            self.driver.find_element_by_xpath("//*[@id=\"user-password\"]//input").clear()
            self.driver.find_element_by_xpath("//*[@id=\"user-password\"]//input").send_keys("Abc1234!")
            self.driver.find_element_by_xpath("//*[@id=\"user-password-confirmation\"]//input").clear()
            self.driver.find_element_by_xpath("//*[@id=\"user-password-confirmation\"]//input").send_keys("Abc1234!")
            self.driver.find_element_by_xpath('//button[contains(.,"Create User")]').click()

            time.sleep(8)
            self.driver.find_element_by_xpath("//button[@label[contains(.,'Go To Account')]]").click()
            if self.is_element_present(By.XPATH,"//*[@id=\"details-tab\"]//p-datatable//table//a[contains(.,'" + randomName + "')]") is False:
                print("ERROR: User was not created")
        except WebDriverException:
            self.fail()

    def test_5_06_searchUsers(self):
        try:
            self.driver.find_element_by_link_text("Users").click()
            self.driver.find_element_by_id("searchInput").send_keys("test")
            self.driver.find_element_by_id("searchInput").send_keys(Keys.ENTER)
            time.sleep(9)
            if (len(self.driver.find_elements_by_xpath("//app-users-list//table//a[contains(.,'test')]")) < 1):
                print("Search did not search the correct string")
        except WebDriverException:
            self.fail()

    def test_5_07_searchWithoutSubAccounts(self):
        try:
            self.driver.find_element_by_xpath("//div[contains(.,'SubAccounts')]//p-checkbox[@name='subAccountsCheckbox']").click()
            self.driver.find_element_by_id("searchInput").send_keys(Keys.ENTER)
            time.sleep(5)
            for i in range(1, 15):
                if self.is_element_present(By.XPATH, "//app-users-list//table//tr[" + str(
                        i) + "]//a[@href[contains(.,'backoffice/accounts/44')]]") is False:
                    print("ERROR: SubAccount included in only Account search!")
        except WebDriverException:
            self.fail()

    def test_5_08_searchCreatedUser(self):
        try:
            self.driver.find_element_by_xpath("//div[contains(.,'SubAccounts')]//p-checkbox[@name='subAccountsCheckbox']").click()
            self.driver.find_element_by_id("searchInput").clear()
            self.driver.find_element_by_id("searchInput").send_keys("testTester" + str(self.randomNum))
            self.driver.find_element_by_id("searchInput").send_keys(Keys.ENTER)
            time.sleep(5)
            self.is_element_present(By.XPATH, "//app-users-list//table//a[contains(.,'" + str(self.randomNum) + "')]")
        except WebDriverException:
            self.fail()

    def test_5_09_changeUserName(self):
        try:
            self.driver.find_element_by_xpath("//app-users-list//table//a[contains(.,'" + str(self.randomNum) + "')]").click()
            if self.is_element_present(By.XPATH, "//label[.='Username']/following-sibling::input") is False:
                self.driver.find_element_by_xpath("//app-users-list//table//a[contains(.,'" + str(self.randomNum) + "')]").click()
            self.driver.find_element_by_xpath("//label[.='Username']/following-sibling::input").send_keys("changed")
            self.driver.find_element_by_xpath("//button//span[contains(.,'Submit')]").click()
            time.sleep(9)
            self.driver.find_element_by_xpath("//button[@label[contains(.,'Go To Account')]]").click()
            time.sleep(2)
            if self.is_element_present(By.XPATH, "//*[@id=\"details-tab\"]//p-datatable//table//a[contains(.,'" + str(
                    self.randomNum) + "changed')]") is False:
                self.assertFalse("ERROR: Username was not updated")
        except WebDriverException:
            self.fail()

    def test_5_10_deleteCreatedUser(self):
        try:
            self.driver.find_element_by_xpath("//*[@id=\"details-tab\"]//p-datatable//table//a[contains(.,'" + str(self.randomNum) + "changed')]").click()
            time.sleep(2)
            self.driver.find_element_by_xpath("//button[@label='Delete']").click()
            self.driver.find_element_by_xpath("//button[span='Yes']").click()
            time.sleep(3)
            self.assertTrue(self.backUrl + "users/list" in self.driver.current_url)
            self.driver.find_element_by_id("searchInput").clear()
            self.driver.find_element_by_id("searchInput").send_keys("testTester" + str(self.randomNum))
            self.driver.find_element_by_id("searchInput").send_keys(Keys.ENTER)
            time.sleep(3)
            self.assertFalse(self.is_element_present(By.XPATH,"//*[@id=\"details-tab\"]//p-datatable//table//a[contains(.,'" + str(
            self.randomNum) + "changed')]"))
        except WebDriverException:
            self.fail()
