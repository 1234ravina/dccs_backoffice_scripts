import os
import datetime
import time
import random
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException


class Utility:
    
            
##############################
# Variables
###############
    driverDir =  str(os.getcwd()) + "\driver\\"
    whichDriver = ["chrome","ie"]
    
    # Base URLs
    #backUrl = "https://vcp.cloud/backoffice/"
    #installPortalUrl = "https://vcp.cloud/installportal/"

    backUrl = "https://qa.vcp-devtest.cloud/backoffice/"
    installPortalUrl = "https://qa.vcp-devtest.cloud/installportal/"
    
    # Credentials for logins
    username = "eng.directed"
    password = "Temp1234!"
    installCred = "dei1"

    # other devices: LNXR-C22LG / ENLR-A22LG / QNVR-C22LG
    activeDeviceID = "1NNR-C22LG"   #R1Z6-0C8C" "1PC2-ALB8"
    deactivatedDeviceID = "1NNR-C22LG" #"R1Z6-0C8C" Device used for install and testing commands in account dccs.automation@gmail.com"
    defectiveDeviceID = "ZLKW-D1U5"
    randomNum = random.randint(0,10000)
    accountToMoveTo = "3000000007" # This is the account we move to to check move device
    partialUserName = "talisker.qa" #this is the partial username of the above account
    currentAccount = "3000000086"   # 389022"  This is the current account number for dccs.automation@gmail.com
    
    # test data - can be used to store values between tests
    # use self.__class__.xxx to save the value
    accountEmail = "dccs.automation@gmail.com" # "mobile@directed.com"
    
    testUser = "maierhoferm.mm@gmail.com"
    testUser30 = "alb"
    
    newUserAccountName = "automationUser"+str(random.randint(0,9999999))
    newUserAccountId = 0

    vehiculeName = ""
    newVehicleName = ""

##############################
# Generic methods
###############
    
    #####
    # Setting up the WebDriver, in this case Chrome
    def setUpActions(self, withLogin=True, whichDriver = whichDriver[0]):
        if whichDriver == "chrome":
            chromeOpts = Options()
            chromeOpts.add_argument("--start-maximized")
            chromeOpts.add_argument("--disable-infobars")
            chromeOpts.add_argument("--disable-print-preview")
            self.driver = webdriver.Chrome(self.driverDir + "chromedriver.exe", options=chromeOpts)
        elif whichDriver == "ie":
            self.driver = webdriver.Ie(self.driverDir + "IEDriverServer.exe")
        self.driver.implicitly_wait(10)
        self.driver.get(self.backUrl + "login")
        
        # if clause for the First Suite that doesn't want the login to be done with no asserts
        if withLogin==True:
            #simple login and navigate to the Search page
            self.driver.find_element_by_id("username").send_keys(self.username)
            self.driver.find_element_by_id("password").send_keys(self.password)
            self.driver.find_element_by_id("login-button").click()

    #####            
    # Quiting the driver
    def tearDownActions(self):
        self.driver.quit()

    #####
    # Return a timestamp
    def timestamp(self):
        return datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d_%H:%M:%S')

    #####
    # To catch a possible element not found exception
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException: return False
        return True
    
    #####
    # Waiting for an URL and return Boolean for asserts
    def wait_for_url(self, url):
        try: Wait(self.driver, 10).until((EC.url_matches(url)))
        except TimeoutException: return False
        return True
