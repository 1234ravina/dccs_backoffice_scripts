import unittest
import time
from time import gmtime, strftime
import random
import os
from selenium.common.exceptions import WebDriverException
from Tests.Utility import Utility
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

# Air ID : 1NNR-C22LG
class Tests_Devices(unittest.TestCase, Utility):

    ##############################
    # Before and after tests
    ###############

    @classmethod
    def setUpClass(inst):
        inst.setUpActions(inst)

    @classmethod
    def tearDownClass(inst):
        inst.tearDownActions(inst)

    ##############################
    # Test cases
    ###############

    #####
    # Navigate to the Search page from the side menu
    def test_4_01_navigateToDevice(self):
        try:
            self.driver.find_element_by_link_text("Devices").click()
            self.assertTrue(self.backUrl + "devices/list" in self.driver.current_url)
        except WebDriverException:
            self.fail()

    #####
    # Verify the autocomplete input is correct
    def test_4_02_deviceLookup(self):
        try:
            deviceSearch = self.driver.find_element_by_xpath("//app-device-search//input")
            deviceSearch.send_keys(self.deactivatedDeviceID[:2])
            searchInput = self.driver.find_element_by_class_name("ui-autocomplete-panel")
            searchButton = self.driver.find_element_by_xpath("//app-device-search/following-sibling::button")

            self.assertFalse(searchButton.is_enabled())
            self.assertFalse(searchInput.is_displayed())

            deviceSearch.send_keys(self.deactivatedDeviceID[2])
            self.assertTrue(self.is_element_present(By.CLASS_NAME, "ui-autocomplete-panel"))
            self.driver.find_element_by_xpath(
                "//*[contains(@class,'ui-autocomplete-panel')]//li[contains(.,'" + self.deactivatedDeviceID + "')]").click()
            searchButton.click()
            Wait(self.driver, 30).until(EC.presence_of_element_located((By.XPATH, "//h4[contains(.,'Device Info')]")))
            airIDinInput = self.driver.find_element_by_xpath(
                "//label[contains(.,'Air ID')]/following-sibling::input").get_attribute('value')
            self.assertEqual(airIDinInput, self.deactivatedDeviceID)
        except WebDriverException:
            self.fail()
    #####
    # Updating the vehicle name but canceling
    def test_4_03_updatingCarCancel(self):
        '''try:
            self.driver.find_element_by_id('details').click()

            # Preparing the variables for the actual update test case
            self.__class__.vehicleName = self.driver.find_element_by_xpath(
                "//label[contains(.,'Vehicle Name')]/following-sibling::input").get_attribute('value')
            self.__class__.newVehicleName = self.__class__.vehicleName
            while self.__class__.newVehicleName == self.__class__.vehicleName:
                self.__class__.newVehicleName = "My Car " + str(random.randint(0, 999))

            nameInput = self.driver.find_element_by_xpath("//label[contains(.,'Vehicle Name')]/following-sibling::input")
            updateButton = self.driver.find_element_by_id('updateButton')
            cancelButton = self.driver.find_element_by_id('cancelButton')

            self.assertFalse(updateButton.is_enabled())
            nameInput.send_keys('test')
            self.assertTrue(updateButton.is_enabled())

            cancelButton.click()
            self.assertFalse(cancelButton.is_enabled())
            Wait(self.driver, 30).until(EC.text_to_be_present_in_element_value(
                (By.XPATH, "//label[contains(.,'Vehicle Name')]/following-sibling::input"), self.__class__.vehicleName))
            time.sleep(20)
        except WebDriverException:
            self.fail()'''

    #####
    # Updating the vehicle name
    def test_4_04_updatingCar(self):
        '''try:
            nameInput = self.driver.find_element_by_xpath("//label[contains(.,'Vehicle Name')]/following-sibling::input")
            nameInput.clear()
            nameInput.send_keys(self.__class__.newVehicleName)
            self.driver.find_element_by_id('updateButton').click()

            # Wait(self.driver,30).until_not(EC.text_to_be_present_in_element_value((By.XPATH,"//label[contains(.,'Vehicle Name')]/following-sibling::input"),self.__class__.newVehicleName))
            Wait(self.driver, 30).until(EC.text_to_be_present_in_element_value(
                (By.XPATH, "//label[contains(.,'Vehicle Name')]/following-sibling::input"), self.__class__.newVehicleName))
            self.assertEqual(nameInput.get_attribute('value'), self.__class__.newVehicleName)
            nameInput.send_keys('My Car')
            self.driver.find_element_by_id('updateButton').click()
        except WebDriverException:
            self.fail()'''

    #####
    # Creating a new vehicle
    def test_4_051_creatingCarCancel(self):
        '''try:
            nameInput = self.driver.find_element_by_xpath("//label[contains(.,'Vehicle Name')]/following-sibling::input")
            updateButton = self.driver.find_element_by_id('updateButton')
            cancelButton = self.driver.find_element_by_id('cancelButton')

            self.driver.find_element_by_id("createAsset").click()
            time.sleep(5)
            self.assertTrue(nameInput.get_attribute('value') == "My Car")
            self.assertTrue(updateButton.is_enabled() & cancelButton.is_enabled())
            self.assertEqual(updateButton.text, "Save")

            nameInput.send_keys(self.__class__.newVehicleName + " New")
            cancelButton.click()

            nameInput.send_keys(" New")
            self.driver.find_element_by_id("createAsset").click()
            updateButton.click()
            self.assertTrue(
                self.driver.find_element_by_xpath("//span[contains(.,'Deactivation Confirmation')]").is_displayed())
            self.driver.find_element_by_xpath("//button[2]/span[2][contains(.,'No')]").click()
            self.assertFalse(
                self.driver.find_element_by_xpath("//span[contains(.,'Deactivation Confirmation')]").is_displayed())
        except WebDriverException:
            self.fail()'''

    def test_4_05_KORECellularInfo(self):
        try:
            self.driver.find_element_by_id('details').click()
            self.driver.find_element_by_xpath("//app-device-detail//button[@label[contains(.,'Get Cellular Info')]]").click()
            time.sleep(5)
            '''phoneElement = self.driver.find_element_by_xpath("//app-device-detail//strong[contains(.,'Phone')]/parent::div").text
            self.assertTrue(phoneElement != "Phone/MDN:", "The KORE Cellular Info was found")'''
        except WebDriverException:
            self.fail()

    #####
    # Test case to confirm motorclub status is updated when device is installed-32966
    def test_4_04_01_Vehicle_Info(self):
        '''try:
            self.driver.find_element_by_id("vehicle-name").clear()
            self.driver.find_element_by_id("data-field").send_keys(self.newVehicleName)
            #self.driver.find_element_by_xpath("//label[contains(.,'VIN')]/following-sibling::input").send_keys(self.VIN)
            #self.driver.find_element_by_name("motorClubStatus")
            #dropdown = Select(self.driver.find_element_by_id("selectPlan"))
            #selectedOption = dropdown.second_selected_option.text
            self.driver.find_element_by_id('updateButton').click()
        except WebDriverException:
            self.fail()'''

    def test_4_06_changeServicePlan(self):
        '''try:
            self.driver.find_element_by_xpath("//*[@id=\"deviceTab\"]/li[3]/a").click()
            self.driver.find_element_by_id("addButton").click()
            time.sleep(3)
            dropdown = Select(self.driver.find_element_by_id("selectPlan"))
            selectedOption = dropdown.first_selected_option.text
            # activeOption = self.driver.find_element_by_xpath("//*[@style=\"color: green;\"]")
            # print((activeOption.text).strip())
            # print(''.join(((activeOption.text).split())))
            # self.assertEqual(selectedOption, activeOption, "Correctly added Service Plan")
            self.driver.find_element_by_id("termButton").click()

            time.sleep(1)
            self.driver.find_element_by_xpath("//app-device-main/p-confirmdialog//button[2]").click()

            if (self.is_element_present(By.XPATH, "//*[@style=\"color: green;\"]") == False):
                self.assertTrue(True, "ERROR: The plan was wrongly terminated!")

            self.driver.find_element_by_id("termButton").click()
            self.driver.find_element_by_xpath("//app-device-main/p-confirmdialog//button[1]").click()
            time.sleep(2)
            if (self.is_element_present(By.XPATH, "//*[@style=\"color: green;\"]") == True):
                self.assertTrue(True, "ERROR: The plan was not terminated!")
            if (self.is_element_present(By.ID, "termButton") == True):
                self.assertTrue(True,
                                "ERROR: The section 'Terminate a Service Plan' is being displayed even after terminating all plans")

            dropdown.select_by_index(4)
            self.driver.find_element_by_id("addButton").click()
            time.sleep(3)
            self.driver.find_element_by_id("termButton").click()
            self.driver.find_element_by_xpath("//app-device-main/p-confirmdialog//button[1]").click()
            time.sleep(2)
            dropdown.select_by_index(4)
            self.driver.find_element_by_id("addButton").click()
            time.sleep(3)
        except WebDriverException:
            self.fail()'''

    def test_4_07_changeNotifications(self):
        '''try:
            self.driver.find_element_by_xpath("//a[contains(.,'Notifications')]").click()
            self.driver.find_element_by_id("defaultButton").click()
            self.assertTrue(self.is_element_present(By.XPATH, "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'Vehicle is moving')]"), "Vehicle is moving alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'Alarm triggered')]"),
                            "Alarm triggered alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'Maximum speed exceeded')]"),
                            "Maximum speed exceeded alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'Battery alert')]"),
                            "Battery alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'Command error at vehicle')]"),
                            "Command error at vehicle alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'Geo-zone2 entry')]"),
                            "Geo-zone2 entry alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'Geo-zone2 exit')]"),
                            "Geo-zone2 exit alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'Geo-zone3 entry')]"),
                            "Geo-zone3 entry alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'Geo-zone3 exit')]"),
                            "Geo-zone3 exit alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'DTC trouble code alert')]"),
                            "DTC trouble code alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'EIPS: Car appears unattended')]"),
                            "EIPS: Car appears unattended alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'EIPS: Trying to shut down')]"),
                            "EIPS: Trying to shut down alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'EIPS: Engine shutdown')]"),
                            "EIPS: Engine shutdown alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'ESP2 Command Error')]"),
                            "ESP2 Command Error alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'ESP2 Alert Triggered')]"),
                            "ESP2 Alert Triggered alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'Pager Command Error')]"),
                            "Pager Command Error alert was set")
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//p-picklist/div[contains(.,'Enabled')]//parent::div/following-sibling::div/descendant::div[contains(.,'Pager Alert Triggered')]"),
                            "Pager Alert Triggered alert was set")
        except WebDriverException:
            self.fail()'''

    def test_4_08_checkEnableBatteryAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Enable Battery Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Enable Battery Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            time.sleep(10)
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'enable_battery')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_09_checkDisableBatteryAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Disable Battery Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Disable Battery Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'disable_battery')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_10_checkEnableMovementAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Enable Movement Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Enable Movement Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'enable_movement')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_11_checkDisableMovementAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Disable Movement Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Disable Movement Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'disable_movement')]"))'''

        except WebDriverException:
            self.fail()

    def test_4_12_checkEnableZone3EntryAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Enable Zone 3 Entry Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Enable Zone 3 Entry Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'enable_zone3_entry')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_13_checkDisableZone3EntryAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Disable Zone 3 Entry Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Disable Zone 3 Entry Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'disable_zone3_entry')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_14_checkEnableZone3ExitAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Enable Zone 3 Exit Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Enable Zone 3 Exit Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'enable_zone3_exit')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_15_checkDisableZone3ExitAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Disable Zone 3 Exit Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Disable Zone 3 Exit Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'disable_zone3_exit')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_16_checkEnableZone2EntryAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Enable Zone 2 Entry Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Enable Zone 2 Entry Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'enable_zone2_entry')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_17_checkDisableZone2EntryAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Disable Zone 2 Entry Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Disable Zone 2 Entry Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'disable_zone2_entry')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_18_checkEnableZone2ExitAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Enable Zone 2 Exit Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Enable Zone 2 Exit Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'enable_zone2_exit')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_19_1_checkDisableZone2ExitAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Disable Zone 2 Exit Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Disable Zone 2 Exit Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'disable_zone2_exit')]"))

            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(40)'''
        except WebDriverException:
            self.fail()
    #####
    # Test exporting CSV file for action history-32049
    def test_4_19_2_checkActionHistory_CSVExport(self):
        '''try:
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            time.sleep(5)

        except WebDriverException:
            self.fail()'''

    def test_4_20_checkManageSchedules(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Create Schedule')]").click()
            self.driver.find_element_by_xpath(
                "//app-device-alert//p-dropdown//label[contains(.,'Select Action')]").click()
            self.driver.find_element_by_xpath(
                "//app-device-alert//p-dropdown//span[contains(.,'Set Speeder Alert')]").click()
            self.driver.find_element_by_xpath("//*[@id='actionParam']//input[@type='number']").send_keys("120")
            self.driver.find_element_by_xpath("//app-device-alert//input[@name='startDate']").send_keys(
                "12/02/2019 12:00")
            time.sleep(10)
            self.driver.find_element_by_xpath("//app-device-alert//input[@name='endDate']").send_keys(
                "12/02/2020 12:00")
            time.sleep(20)

            # self.driver.find_element_by_id("data-field").send_keys("09:23 PM")
            self.driver.find_element_by_xpath(
                "//app-device-alert//p-dropdown//label[contains(.,'Select Type')]").click()
            self.driver.find_element_by_xpath("//app-device-alert//p-dropdown//span[contains(.,'Every Day')]").click()
            self.driver.find_element_by_xpath("//button[@id='createButton'][contains(.,'Submit')]").click()

            '''self.driver.find_element_by_id("backButton").click()
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'2018-09-01')]/ancestor::tr//span[contains(.,'set_speed_alert')]"))
            self.driver.find_element_by_xpath("//app-standard-editing//button").click()
            time.sleep(10)'''

        except WebDriverException:
            self.fail()

    def test_4_21_actionHistory(self):
        '''try:
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            time.sleep(5)
            self.driver.find_element_by_xpath("//*[@id=\"fields-selector\"]/div/div[3]/span").click()
            self.driver.find_element_by_xpath("//label[contains(.,'Fix Status')]").click()
            self.driver.find_element_by_xpath("//*[@id=\"fields-selector\"]/div/div[3]/span").click()
            self.driver.find_element_by_id("searchButton").click()
            if self.is_element_present(By.XPATH, "//span[contains(.,'Fix Status')]") is False:
                self.assertFalse("The column Fix Status was not added")
            self.driver.find_element_by_xpath("//*[@id=\"fields-selector\"]/div/div[3]/span").click()
            self.driver.find_element_by_xpath("//*[@id=\"fields-selector\"]/div/div[4]/div[1]/div[1]/div[2]").click()
            self.driver.find_element_by_xpath("//*[@id=\"fields-selector\"]/div/div[4]/div[1]/div[1]/div[2]").click()
            if self.is_element_present(By.XPATH, "//label[contains(.,'Choose at least one')]") is False:
                self.assertFalse("All the columns were not deselected")
            self.driver.find_element_by_xpath("//*[@id=\"fields-selector\"]/div/div[3]/span").click()
            self.driver.find_element_by_xpath("//*[@id=\"resetDisplayColumns\"]").click()
        except WebDriverException:
            self.fail()'''

    def test_4_22_managementTab(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Management')]").click()

            #####
            ## Exchange With Another Device
            self.driver.find_element_by_xpath("//*[@id='exchangeInput']").send_keys("NP9U-6B0JJ")

            self.driver.find_element_by_id("getAirId").click()
            time.sleep(8)

            self.driver.find_element_by_id("exchangeButton")
            time.sleep(5)

            #####
            ## Move Device to Another Account
            self.driver.find_element_by_xpath("//app-device-management//b[contains(.,'Account ID')]/following-sibling::input").send_keys("5678")

            self.driver.find_element_by_id("moveButton").click()

        except WebDriverException:
            self.fail()

    # Commenting this out as this functionality is failing as of 10/05/2018
    '''def test_4_231_deactivateDevice(self):
        self.driver.find_element_by_id("deactivateDeviceButton").click()
        time.sleep(1)
        if self.is_element_present(By.ID, "deactivateDeviceButton"):
            self.assertFalse("Failed: The device was not deactivated")
        self.driver.find_element_by_id("activateDeviceButton").click()
        time.sleep(1)
        if self.is_element_present(By.ID, "activateDeviceButton"):
            self.assertFalse("Failed: The device was not activated")'''

    def test_4_23_01_testingTab_PingDevice(self):
        try:
            time.sleep(60)
            self.driver.find_element_by_xpath("//a[contains(.,'Testing')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Ping Device')]").click()
            Wait(self.driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Ping Device')]")))
            currentTime = strftime("%H", time.localtime())
            time.sleep(10)
            '''if self.is_element_present(By.XPATH,
                                       "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span[contains(.,'locate']") is False:
                self.assertFalse("The Ping Device action " + currentTime + " was not correctly recorded in the table.")'''
        except WebDriverException:
            self.fail()

    def test_4_23_02_GetDevice_Status(self):
        '''try:
            time.sleep(50)
            self.driver.find_element_by_xpath("//a[contains(.,'Get Device Status')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Get Device Status')]").click()
            Wait(self.driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Get Device Status')]")))
            currentTime = strftime("%H", time.localtime())
            time.sleep(10)

            if self.is_element_present(By.XPATH,
                                       "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span[contains(.,'locate']") is False:
                self.assertFalse("The Ping Device action " + currentTime + " was not correctly recorded in the table.")
        except WebDriverException:
            self.fail()'''

    def test_4_24_testingTab_RemoteStart(self):
        '''try:
            self.driver.find_element_by_xpath("//button[contains(.,'Remote Start')]").click()
            Wait(self.driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Remote Start')]")))
            currentTime = strftime("%H", time.localtime())
            time.sleep(30)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span[contains(.,'remote']"))
            self.driver.find_element_by_xpath("//button[contains(.,'Remote Start')]").click()
            Wait(self.driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Remote Start')]")))
        except WebDriverException:
            self.fail()'''

    def test_4_25_testingTab_DoorLock(self):
        try:
            self.driver.find_element_by_xpath("//button[contains(.,'Door Lock')]").click()
            Wait(self.driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Door Lock')]")))
            currentTime = strftime("%H", time.localtime())
            time.sleep(15)
            '''self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span[contains(.,'lock']"))'''
        except WebDriverException:
            self.fail()

    def test_4_26_testingTab_DoorUnlock(self):
        try:
            self.driver.find_element_by_xpath("//button[contains(.,'Door Unlock')]").click()
            Wait(self.driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Door Unlock')]")))
            currentTime = strftime("%H", time.localtime())
            time.sleep(15)
            '''self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span[contains(.,'unlock']"))'''
        except WebDriverException:
            self.fail()

    def test_4_27_testingTab_Trunk(self):
        try:
            self.driver.find_element_by_xpath("//button[contains(.,'Trunk')]").click()
            Wait(self.driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Trunk')]")))
            currentTime = strftime("%H", time.localtime())
            time.sleep(15)
            '''self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span[contains(.,'trunk']"))'''
        except WebDriverException:
            self.fail()

    def test_4_28_testingTab_Panic(self):
        try:
            self.driver.find_element_by_xpath("//button[contains(.,'Panic')]").click()
            Wait(self.driver, 60).until(EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Panic')]")))
            currentTime = strftime("%H", time.localtime())
            time.sleep(15)
            '''self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span[contains(.,'panic']"))
            self.driver.find_element_by_xpath("//button[contains(.,'Door Unlock')]").click()'''
        except WebDriverException:
            self.fail()

    def test_4_29_testingTab_Aux1(self):
        try:
            self.driver.find_element_by_xpath("//button[contains(.,'Auxiliary No. 1')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Auxiliary No. 1')]")))
            currentTime = strftime("%H", time.localtime())
            time.sleep(15)
            '''self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span[contains(.,'aux2']"))'''
        except WebDriverException:
            self.fail()

    def test_4_30_testingTab_Aux2(self):
        try:
            self.driver.find_element_by_xpath("//button[contains(.,'Auxiliary No. 2')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Auxiliary No. 2')]")))
            currentTime = strftime("%H", time.localtime())
            time.sleep(15)
            '''self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span[contains(.,'aux3']"))'''
        except WebDriverException:
            self.fail()

    def test_4_31_DeviceConfiguration(self):
        try:
            time.sleep(6)
            self.driver.find_element_by_xpath("//a[contains(.,'Testing')]").click()
            self.driver.find_element_by_xpath("//h4[contains(.,'Device Configuration')]//i").click()
            self.driver.find_element_by_xpath("//app-device-testing//p-dropdown[@name='lockConfig']//label").click()
            self.driver.find_element_by_xpath("//app-device-testing//p-dropdown[@name='lockConfig']//span[contains(.,'Analog Wire')]").click()
            time.sleep(1)
            self.assertTrue(self.driver.find_element_by_xpath("//span[contains(.,'Device Configuration')]/following-sibling::div/descendant::button[@label='Cancel']").is_enabled())
            self.driver.find_element_by_xpath("//app-device-testing//p-dropdown[@name='lockConfigAnalog']//label").click()
            self.driver.find_element_by_xpath("//app-device-testing//p-dropdown[@name='lockConfigAnalog']//span[contains(.,'single pulse 0.4 sec')]").click()
            time.sleep(1)
            self.assertTrue(self.driver.find_element_by_xpath("//span[contains(.,'Device Configuration')]/following-sibling::div/descendant::button[@label='Save']").is_enabled())

        except WebDriverException:
            self.fail()

    def test_4_32_DeviceConfigurationSaveAndRevert(self):
        try:
            self.driver.find_element_by_xpath("//span[contains(.,'Device Configuration')]/following-sibling::div/descendant::button[@label='Save']").click()
            time.sleep(1)
            self.driver.find_element_by_xpath("//app-device-testing//p-dropdown[@name='lockConfig']//label").click()
            self.driver.find_element_by_xpath("//app-device-testing//p-dropdown[@name='lockConfig']//span[contains(.,'Data Connection')]").click()
            time.sleep(1)
            self.assertTrue(self.driver.find_element_by_xpath("//span[contains(.,'Device Configuration')]/following-sibling::div/descendant::button[@label='Save']").is_enabled())
            self.driver.find_element_by_xpath("//span[contains(.,'Device Configuration')]/following-sibling::div/descendant::button[@label='Save']").click()
            time.sleep(15)
        except WebDriverException:
            self.fail()

    def test_4_33_FleetConfiguration(self):

            ### Driver Behavior, Fleet, Impact Detection
            self.driver.find_element_by_xpath("//span[contains(text(),'Get Fleet Configuration')]").click()
            time.sleep(8)


    def test_4_34_DeleteDevice(self):
        try:
            time.sleep(5)
            self.driver.find_element_by_xpath("//a[contains(.,'Management')]").click()
            self.driver.find_element_by_id("removeDeviceButton").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Yes')]").click()
            time.sleep(5)
        except WebDriverException:
            self.fail()






