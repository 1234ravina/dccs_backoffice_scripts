import unittest
import time
from time import gmtime, strftime
import random
import os
from selenium.common.exceptions import WebDriverException
from Tests.Utility import Utility
from selenium.webdriver.support.ui import WebDriverWait as Wait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select

class Tests_Devices(unittest.TestCase, Utility):

    ##############################
    # Before and after tests
    ###############

    @classmethod
    def setUpClass(inst):
        inst.setUpActions(inst)

    @classmethod
    def tearDownClass(inst):
        inst.tearDownActions(inst)

    ##############################
    # Test cases
    ###############

    #####
    # Navigate to the Search page from the side menu
    def test_4_01_navigateToDevice(self):
        try:
            self.driver.find_element_by_link_text("Devices").click()
            self.assertTrue(self.backUrl + "/devices/list" in self.driver.current_url)
        except WebDriverException:
            self.fail()

    def test_4_02_deviceLookup(self):
        try:
            self.driver.find_element_by_xpath("//app-device-search//input")
            self.driver.find_element_by_id("searchInput").send_keys("1NNR")
            self.driver.find_element_by_id("searchButton").click()
            self.driver.find_element_by_xpath("//*[@id='content']/div/app-devices/div[3]/app-devices-list/p-table/div/div/table/tbody/tr[1]/td[1]/a").click()
        except WebDriverException:
            self.fail()
    #####
    # Updating the vehicle name but canceling
    def test_4_03_Details_UpdateDevice(self):
        try:
            self.driver.find_element_by_id("details").click()
            self.driver.find_element_by_xpath("//app-device-detail//button[@label[contains(.,'Update Device')]]").click()
            time.sleep(5)
        except WebDriverException:
            self.fail()

    def test_4_04_KORECellularInfo(self):
        try:
            self.driver.find_element_by_id("details").click()
            self.driver.find_element_by_xpath("//app-device-detail//button[@label[contains(.,'Get Cellular Info')]]").click()
            time.sleep(5)
        except WebDriverException:
            self.fail()

    #####
    # Creating a new vehicle and Cancel
    def test_4_051_creatingCarCancel(self):
        try:
                self.driver.find_element_by_xpath("//*[@id='createAsset']/span").click()
                self.driver.find_element_by_xpath(
                    "//app-input-field[@id='vehicle-name']//div//input[@id='data-field']").clear()
                self.driver.find_element_by_xpath(
                    "//app-input-field[@id='vehicle-name']//div//input[@id='data-field']").send_keys("MyCar")
                time.sleep(0.1)
                self.driver.find_element_by_xpath(
                    "//app-input-field[@name='vin']//div//input[@id='data-field']").clear()
                self.driver.find_element_by_xpath(
                    "//app-input-field[@name='vin']//div//input[@id='data-field']").send_keys("1HGBH41JXMN109186")
                time.sleep(0.2)
                self.driver.find_element_by_xpath(
                    "//app-input-field[@name='vehicleYear']//div//input[@id='data-field']").clear()
                self.driver.find_element_by_xpath(
                    "//app-input-field[@name='vehicleYear']//div//input[@id='data-field']").send_keys("2019")
                time.sleep(0.1)
                self.driver.find_element_by_xpath(
                    "//app-input-field[@name='vehicleMake']//div//input[@id='data-field']").clear()
                self.driver.find_element_by_xpath(
                    "//app-input-field[@name='vehicleMake']//div//input[@id='data-field']").send_keys("Ferrari")
                time.sleep(0.1)
                self.driver.find_element_by_xpath(
                    "//app-input-field[@name='vehicleModel']//div//input[@id='data-field']").clear()
                self.driver.find_element_by_xpath(
                    "//app-input-field[@name='vehicleModel']//div//input[@id='data-field']").send_keys("ram")
                time.sleep(0.1)
                self.driver.find_element_by_xpath(
                    "//app-input-field[@name='auxButtonName']//div//input[@id='data-field']").clear()
                self.driver.find_element_by_xpath(
                    "//app-input-field[@name='auxButtonName']//div//input[@id='data-field']").send_keys("Door")
                time.sleep(0.1)
                self.driver.find_element_by_id("cancelButton").click()
        except WebDriverException:
                self.fail()

    #####
    # Update Vehicle Info
    def test_4_05_Vehicle_Info(self):
            self.driver.find_element_by_xpath("//app-input-field[@id='vehicle-name']//div//input[@id='data-field']").clear()
            self.driver.find_element_by_xpath(
                "//app-input-field[@id='vehicle-name']//div//input[@id='data-field']").send_keys("MyNewCar")
            time.sleep(0.1)
            self.driver.find_element_by_xpath("//app-input-field[@name='vin']//div//input[@id='data-field']").clear()
            self.driver.find_element_by_xpath(
                "//app-input-field[@name='vin']//div//input[@id='data-field']").send_keys("1HGBH41JXMN109186")
            time.sleep(0.2)
            self.driver.find_element_by_xpath("//app-input-field[@name='vehicleYear']//div//input[@id='data-field']").clear()
            self.driver.find_element_by_xpath(
                "//app-input-field[@name='vehicleYear']//div//input[@id='data-field']").send_keys("2020")
            time.sleep(0.1)
            self.driver.find_element_by_xpath("//app-input-field[@name='vehicleMake']//div//input[@id='data-field']").clear()
            self.driver.find_element_by_xpath(
                "//app-input-field[@name='vehicleMake']//div//input[@id='data-field']").send_keys("Ford")
            time.sleep(0.1)
            self.driver.find_element_by_xpath("//app-input-field[@name='vehicleModel']//div//input[@id='data-field']").clear()
            self.driver.find_element_by_xpath(
                "//app-input-field[@name='vehicleModel']//div//input[@id='data-field']").send_keys("ram")
            time.sleep(0.1)
            self.driver.find_element_by_xpath("//app-input-field[@name='auxButtonName']//div//input[@id='data-field']").clear()
            self.driver.find_element_by_xpath(
                "//app-input-field[@name='auxButtonName']//div//input[@id='data-field']").send_keys("Window")
            time.sleep(0.1)
            ## Test case to confirm MotorClub status is updated when device is installed-32966
            self.driver.find_element_by_xpath("/html[1]/body[1]/app-root[1]/div[1]/div[2]/div[2]/div[1]/div[1]/app-devices[1]/div[3]/app-device-main[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/app-device-detail[1]/div[1]/div[3]/div[1]/form[1]/div[5]/div[1]/p-dropdown[1]/div[1]/div[3]/label[1]").click()
            time.sleep(0.1)
            self.driver.find_element_by_xpath("/html[1]/body[1]/app-root[1]/div[1]/div[2]/div[2]/div[1]/div[1]/app-devices[1]/div[3]/app-device-main[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[2]/app-device-detail[1]/div[1]/div[3]/div[1]/form[1]/div[5]/div[1]/p-dropdown[1]/div[1]/div[5]/div[1]/ul[1]/p-dropdownitem[4]/li[1]/span[1]").click()
            time.sleep(0.1)
            self.driver.find_element_by_xpath("//span[text()='Update']").click()

    def test_4_06_changeServicePlan(self):
        try:
            self.driver.find_element_by_xpath("//*[@id=\"deviceTab\"]/li[3]/a").click()
            # Auto Renew Button check
            self.driver.find_element_by_xpath("/html[1]/body[1]/app-root[1]/div[1]/div[2]/div[2]/div[1]/div[1]/app-devices[1]/div[3]/app-device-main[1]/div[1]/div[1]/div[2]/div[1]/div[2]/div[3]/app-device-service-plan[1]/div[1]/div[1]/div[1]/div[1]/button[1]/span[1]").click()
            time.sleep(0.1)
            self.driver.find_element_by_id("addButton").click()
            time.sleep(3)
            dropdown = Select(self.driver.find_element_by_id("selectPlan"))
            selectedOption = dropdown.first_selected_option.text
            self.driver.find_element_by_id("termButton").click()
            time.sleep(1)
            self.driver.find_element_by_xpath("//app-device-main/p-confirmdialog//button[2]").click()
            if (self.is_element_present(By.XPATH, "//*[@style=\"color: green;\"]") == False):
                self.assertTrue(True, "ERROR: The plan was wrongly terminated!")
            self.driver.find_element_by_id("termButton").click()
            self.driver.find_element_by_xpath("//app-device-main/p-confirmdialog//button[1]").click()
            time.sleep(2)
            if (self.is_element_present(By.XPATH, "//*[@style=\"color: green;\"]") == True):
                self.assertTrue(True, "ERROR: The plan was not terminated!")
            if (self.is_element_present(By.ID, "termButton") == True):
                self.assertTrue(True,
                                "ERROR: The section 'Terminate a Service Plan' is being displayed even after terminating all plans")
            dropdown.select_by_index(4)
            self.driver.find_element_by_id("addButton").click()
            time.sleep(3)
            self.driver.find_element_by_id("termButton").click()
            self.driver.find_element_by_xpath("//app-device-main/p-confirmdialog//button[1]").click()
            time.sleep(2)
            dropdown.select_by_index(4)
            self.driver.find_element_by_id("addButton").click()
            time.sleep(3)
        except WebDriverException:
            self.fail()

    def test_4_07_changeNotifications(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Notifications')]").click()
            self.driver.find_element_by_id("defaultButton").click()
            time.sleep(0.2)
            #self.driver.find_element_by_xpath("//span[contains(text(),'Update Profile')]").click()
            #time.sleep(0.1)
        except WebDriverException:
            self.fail()

    def test_4_08_checkEnableBatteryAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Enable Battery Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Enable Battery Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            time.sleep(10)
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'enable_battery')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_09_checkDisableBatteryAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Disable Battery Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Disable Battery Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'disable_battery')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_10_checkEnableMovementAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Enable Movement Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Enable Movement Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'enable_movement')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_11_checkDisableMovementAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Disable Movement Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Disable Movement Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'disable_movement')]"))'''

        except WebDriverException:
            self.fail()

    def test_4_12_checkEnableZone3EntryAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Enable Zone 3 Entry Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Enable Zone 3 Entry Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'enable_zone3_entry')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_13_checkDisableZone3EntryAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Disable Zone 3 Entry Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Disable Zone 3 Entry Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'disable_zone3_entry')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_14_checkEnableZone3ExitAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Enable Zone 3 Exit Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Enable Zone 3 Exit Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'enable_zone3_exit')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_15_checkDisableZone3ExitAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Disable Zone 3 Exit Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Disable Zone 3 Exit Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'disable_zone3_exit')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_16_checkEnableZone2EntryAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Enable Zone 2 Entry Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Enable Zone 2 Entry Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'enable_zone2_entry')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_17_checkDisableZone2EntryAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Disable Zone 2 Entry Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Disable Zone 2 Entry Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'disable_zone2_entry')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_18_checkEnableZone2ExitAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Enable Zone 2 Exit Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Enable Zone 2 Exit Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'enable_zone2_exit')]"))'''
        except WebDriverException:
            self.fail()

    def test_4_19_checkDisableZone2ExitAlert(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Disable Zone 2 Exit Alert')]").click()
            Wait(self.driver, 60).until(
                EC.element_to_be_clickable((By.XPATH, "//button[contains(.,'Disable Zone 2 Exit Alert')]")))
            '''currentTime = strftime("%H:%M", time.localtime())
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(10)
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'" + currentTime + "')]/ancestor::tr//span//span[contains(.,'disable_zone2_exit')]"))

            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(40)'''
        except WebDriverException:
            self.fail()

    def test_4_20_checkActionHistory(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            self.driver.find_element_by_id("searchButton").click()
            time.sleep(3)
        except WebDriverException:
            self.fail()

    def test_4_21_checkManageSchedules(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Alerts')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Create Schedule')]").click()
            self.driver.find_element_by_xpath("//app-device-alert//p-dropdown//label[contains(.,'Select Action')]").click()
            self.driver.find_element_by_xpath("//app-device-alert//p-dropdown//span[contains(.,'Set Speeder Alert')]").click()
            self.driver.find_element_by_xpath("//*[@id='actionParam']//input[@type='number']").send_keys("120")
            self.driver.find_element_by_xpath("//app-device-alert//input[@name='startDate']").send_keys("12/02/2019 12:00")
            time.sleep(10)
            self.driver.find_element_by_xpath("//app-device-alert//input[@name='endDate']").send_keys("12/02/2020 12:00")
            time.sleep(20)

            # self.driver.find_element_by_id("data-field").send_keys("09:23 PM")
            self.driver.find_element_by_xpath("//app-device-alert//p-dropdown//label[contains(.,'Select Type')]").click()
            self.driver.find_element_by_xpath("//app-device-alert//p-dropdown//span[contains(.,'Every Day')]").click()
            self.driver.find_element_by_xpath("//button[@id='createButton'][contains(.,'Submit')]").click()

            '''self.driver.find_element_by_id("backButton").click()
            self.assertTrue(self.is_element_present(By.XPATH,
                                                    "//table//tbody//span[contains(.,'2018-09-01')]/ancestor::tr//span[contains(.,'set_speed_alert')]"))
            self.driver.find_element_by_xpath("//app-standard-editing//button").click()
            time.sleep(10)'''
        except WebDriverException:
            self.fail()

    def test_4_22_actionHistory(self):
        '''try:
            self.driver.find_element_by_xpath("//a[contains(.,'Action History')]").click()
            time.sleep(5)
            self.driver.find_element_by_xpath("//*[@id=\"fields-selector\"]/div/div[3]/span").click()
            self.driver.find_element_by_xpath("//label[contains(.,'Fix Status')]").click()
            self.driver.find_element_by_xpath("//*[@id=\"fields-selector\"]/div/div[3]/span").click()
            self.driver.find_element_by_id("searchButton").click()
            if self.is_element_present(By.XPATH, "//span[contains(.,'Fix Status')]") is False:
                self.assertFalse("The column Fix Status was not added")
            self.driver.find_element_by_xpath("//*[@id=\"fields-selector\"]/div/div[3]/span").click()
            self.driver.find_element_by_xpath("//*[@id=\"fields-selector\"]/div/div[4]/div[1]/div[1]/div[2]").click()
            self.driver.find_element_by_xpath("//*[@id=\"fields-selector\"]/div/div[4]/div[1]/div[1]/div[2]").click()
            if self.is_element_present(By.XPATH, "//label[contains(.,'Choose at least one')]") is False:
                self.assertFalse("All the columns were not deselected")
            self.driver.find_element_by_xpath("//*[@id=\"fields-selector\"]/div/div[3]/span").click()
            self.driver.find_element_by_xpath("//*[@id=\"resetDisplayColumns\"]").click()
        except WebDriverException:
            self.fail()'''

    def test_4_23_ManagementTab(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Management')]").click()
            #####
            ## Exchange With Another Device
            self.driver.find_element_by_xpath("//*[@id='exchangeInput']").send_keys("NP9U-6B0JJ")
            self.driver.find_element_by_id("getAirId").click()
            time.sleep(8)
            self.driver.find_element_by_id("exchangeButton")
            time.sleep(5)

            #####
            ## Move Device to Another Account
            self.driver.find_element_by_xpath("//app-device-management//b[contains(.,'Account ID')]/following-sibling::input").send_keys("5678")
            self.driver.find_element_by_id("moveButton").click()
        except WebDriverException:
            self.fail()

    # Commenting this out as this functionality is failing as of 10/05/2018
    '''def test_4_231_deactivateDevice(self):
        self.driver.find_element_by_id("deactivateDeviceButton").click()
        time.sleep(1)
        if self.is_element_present(By.ID, "deactivateDeviceButton"):
            self.assertFalse("Failed: The device was not deactivated")
        self.driver.find_element_by_id("activateDeviceButton").click()
        time.sleep(1)
        if self.is_element_present(By.ID, "activateDeviceButton"):
            self.assertFalse("Failed: The device was not activated")'''

    def test_4_24_testingTab_DoorLock(self):
        try:
            time.sleep(5)
            self.driver.find_element_by_xpath("//a[contains(.,'Testing')]").click()
            self.driver.find_element_by_xpath("//span[contains(text(),'Door Lock')]").click()
            time.sleep(30)
        except WebDriverException:
            self.fail()

    def test_4_25_testingTab_DoorUnlock(self):
        try:
            time.sleep(30)
            self.driver.find_element_by_xpath("//button[contains(.,'Door Unlock')]").click()
            time.sleep(30)
        except WebDriverException:
            self.fail()

    def test_4_26_testingTab_Trunk(self):
        try:
            self.driver.find_element_by_xpath("//button[contains(.,'Trunk')]").click()
            time.sleep(30)
        except WebDriverException:
            self.fail()

    def test_4_27_testingTab_Panic(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Testing')]").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Panic')]").click()
            time.sleep(30)
        except WebDriverException:
            self.fail()

    def test_4_28_testingTab_Aux1(self):
        try:
            self.driver.find_element_by_xpath("//button[contains(.,'Auxiliary No. 1')]").click()
            time.sleep(30)
        except WebDriverException:
            self.fail()

    def test_4_29_testingTab_Aux2(self):
        try:
            time.sleep(30)
            self.driver.find_element_by_xpath("//button[contains(.,'Auxiliary No. 2')]").click()
            time.sleep(30)
        except WebDriverException:
            self.fail()

    def test_4_30_testingTab_PingDevice(self):
        try:
            time.sleep(10)
            self.driver.find_element_by_xpath("//button[contains(.,'Ping Device')]").click()
        except WebDriverException:
            self.fail()

    def test_4_31_GetDevice_Status(self):
        try:
            time.sleep(10)
            self.driver.find_element_by_xpath("//span[contains(text(),'Get Device Status')]").click()
            time.sleep(10)
        except WebDriverException:
            self.fail()

    def test_4_32_testingTab_RemoteStart(self):
        try:
            self.driver.find_element_by_xpath("//span[contains(text(),'Remote Start')]").click()
            time.sleep(10)
        except WebDriverException:
            self.fail()

    def test_4_33_DeviceConfiguration(self):
        try:
            time.sleep(6)
            self.driver.find_element_by_xpath("//a[contains(.,'Testing')]").click()
            self.driver.find_element_by_xpath("//h4[contains(.,'Device Configuration')]//i").click()
            self.driver.find_element_by_xpath("//app-device-testing//p-dropdown[@name='lockConfig']//label").click()
            self.driver.find_element_by_xpath("//app-device-testing//p-dropdown[@name='lockConfig']//span[contains(.,'Analog Wire')]").click()
            time.sleep(1)
            self.assertTrue(self.driver.find_element_by_xpath("//span[contains(.,'Device Configuration')]/following-sibling::div/descendant::button[@label='Cancel']").is_enabled())
            self.driver.find_element_by_xpath("//app-device-testing//p-dropdown[@name='lockConfigAnalog']//label").click()
            self.driver.find_element_by_xpath("//app-device-testing//p-dropdown[@name='lockConfigAnalog']//span[contains(.,'single pulse 0.4 sec')]").click()
            time.sleep(1)
            self.assertTrue(self.driver.find_element_by_xpath("//span[contains(.,'Device Configuration')]/following-sibling::div/descendant::button[@label='Save']").is_enabled())
        except WebDriverException:
            self.fail()

    def test_4_34_DeviceConfigurationSaveAndRevert(self):
        try:
            self.driver.find_element_by_xpath("//span[contains(.,'Device Configuration')]/following-sibling::div/descendant::button[@label='Save']").click()
            time.sleep(1)
            self.driver.find_element_by_xpath("//app-device-testing//p-dropdown[@name='lockConfig']//label").click()
            self.driver.find_element_by_xpath("//app-device-testing//p-dropdown[@name='lockConfig']//span[contains(.,'Data Connection')]").click()
            time.sleep(1)
            self.assertTrue(self.driver.find_element_by_xpath("//span[contains(.,'Device Configuration')]/following-sibling::div/descendant::button[@label='Save']").is_enabled())
            self.driver.find_element_by_xpath("//span[contains(.,'Device Configuration')]/following-sibling::div/descendant::button[@label='Save']").click()
            time.sleep(15)
        except WebDriverException:
            self.fail()

    def test_4_35_SendAmazonKeyCommands(self):
            ### Send Amazon Key Commands
            self.driver.find_element_by_xpath("//span[contains(text(),'Execute Command')]").click()
            time.sleep(8)

    def test_4_36_FleetConfiguration(self):
            ### Driver Behavior, Fleet, Impact Detection
            self.driver.find_element_by_xpath("//span[contains(text(),'Get Fleet Configuration')]").click()
            time.sleep(8)

    def test_4_27_smsCommand_Reboot(self):
        try:
            self.driver.find_element_by_xpath("//a[contains(.,'Configuration')]").click()
            time.sleep(0.5)
            self.driver.find_element_by_xpath("//span[contains(text(),'Reboot')]").click()
            time.sleep(0.2)
        except WebDriverException:
            self.fail()

    def test_4_38_DeleteDevice(self):
        try:
            time.sleep(5)
            self.driver.find_element_by_xpath("//a[contains(.,'Management')]").click()
            self.driver.find_element_by_id("removeDeviceButton").click()
            self.driver.find_element_by_xpath("//button[contains(.,'Yes')]").click()
            time.sleep(5)
        except WebDriverException:
            self.fail()






